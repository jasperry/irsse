#!/bin/bash

# find common words in first field of two files

cat "$1" | awk -F ':' '{print $1}' | sort > /tmp/common_left.words

cat "$2" | awk -F ':' '{print $1}' | sort > /tmp/common_right.words

comm -1 -2 /tmp/common_left.words /tmp/common_right.words

