#!/bin/bash

# java -Xmx2500M -cp ./bin/ edu.rutgers.kevin.sse.CountAttack
# java -Xmx5000M -cp "lib/*:./build/classes/" edu.rutgers.kevin.sse.CountAttack
java -Xmx5000M -cp "lib/*:./bin" edu.rutgers.kevin.sse.CountAttack
