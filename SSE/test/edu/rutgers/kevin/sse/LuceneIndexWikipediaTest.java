package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/**
 * @brief Use the wikixmlj Wikipedia dump parser to index documents into Lucene
 *        as they are parsed.
 * @author paulgrubbs
 * 
 */
public class LuceneIndexWikipediaTest {
	private static final String PARSED_ARTICLES_DIRECTORY = "/Users/paulgrubbs/data/enwiki/articles";
	private static final String LUCENE_DIR = "/Users/paulgrubbs/data/enwiki/lucene-dir";
	static String file = "/Users/paulgrubbs/data/enwiki-20150403-pages-articles.xml";
	static AtomicLong ctr = new AtomicLong();
	static long prevMillis;
	static long numProcessed = 0;
	static int maxProcessed = 5000;
	static long printAfterThisManyDocuments = 5000;
	static long beforeTime = System.currentTimeMillis();
	private static final Logger log = Logger
			.getLogger(LuceneIndexWikipediaTest.class);
	private static final long processThisMany = 100000;

	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");
		writeIndexFromIndividualFiles(PARSED_ARTICLES_DIRECTORY, LUCENE_DIR);
		search("religion");
	}

	/**
	 * @brief Search for a word using the Lucene index that was written and
	 *        print the titles of the results.
	 */
	private static void search(String search) {
		IndexSearcher searcher = null;
		QueryParser parser = null;
		try {
			searcher = new IndexSearcher(DirectoryReader.open(FSDirectory
					.open(new File(LUCENE_DIR))));
			parser = new QueryParser(Version.LUCENE_4_9, "text",
					new WikipediaAnalyzer());

			Query q = parser.parse(search);
			TopDocs td = searcher.search(q, 1500);
			ScoreDoc[] hits = td.scoreDocs;

			for (int i = 0; i < hits.length; i++) {
				org.apache.lucene.document.Document d = searcher
						.doc(hits[i].doc);
				System.out.println(d.get("title"));
			}
			System.out.println("Search for " + search + " returned "
					+ hits.length + " results.");
		} catch (IOException | ParseException e) {
			log.error("Exception!", e);
		}
	}

	private static void writeIndexFromIndividualFiles(
			String pathToIndividualFiles, String pathToLuceneDir) {
		IndexWriter idx = null;
		try {
			File lf = new File(pathToLuceneDir);
			lf.mkdirs();
			Directory d = FSDirectory.open(lf);
			IndexWriterConfig config = new IndexWriterConfig(
					Version.LUCENE_4_9, new WikipediaAnalyzer());
			idx = new IndexWriter(d, config);
			parseDocumentsAtLocation(new File(pathToIndividualFiles),
					new LuceneIndexerFileCallbackHandler(idx));
		} catch (IOException e) {
			log.error("Exception!", e);
		} finally {
			try {
				idx.close();
			} catch (IOException e) {
				log.error("Exception!", e);
			}
		}
	}

	private static void parseDocumentsAtLocation(File node,
			FileCallbackHandler fch) {
		if (numProcessed >= processThisMany) {
			return;
		}
		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				parseDocumentsAtLocation(new File(node, filename), fch);
			}
		} else {
			
				numProcessed++;
				fch.process(node);
			
			if (numProcessed % printAfterThisManyDocuments == 0) {
				System.out.println("Processed an additional "
						+ printAfterThisManyDocuments + " documents for "
						+ numProcessed + " total documents in "
						+ ((System.nanoTime() - beforeTime) / 1000000)
						+ " milliseconds. ");
				beforeTime = System.nanoTime();
			}

		}

	}

}
