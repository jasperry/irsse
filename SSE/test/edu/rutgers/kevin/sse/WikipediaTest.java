package edu.rutgers.kevin.sse;


import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import edu.jhu.nlp.wikipedia.WikiXMLParser;
import edu.jhu.nlp.wikipedia.WikiXMLParserFactory;

public class WikipediaTest {
	static String file = "/Users/paulgrubbs/data/enwiki-20150403-pages-articles.xml";
	static AtomicLong ctr = new AtomicLong();
	static long prevMillis;
	static int maxProcessed = 5000;
	static long numProcessed = 0;
	private static final Logger log = Logger.getLogger(WikipediaTest.class);

	

	public static void main(String[] args) {
		PropertyConfigurator.configure("log4j.properties");

		parseAndWriteFiles(file);

	}
	/**
	 * @brief Write the contents of a single Wikipedia article to its own file on the file system.
	 */
	private static void parseAndWriteFiles(String dump_file) {
		WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser(dump_file);
		long beforeEverythingMillis = 0;

		try {
			prevMillis = System.currentTimeMillis();
			beforeEverythingMillis = prevMillis;
			wxsp.setPageCallback(new ArticleWriterPageCallbackHandler(
					"/Users/paulgrubbs/data/enwiki/articles", ctr, maxProcessed));

			wxsp.parse();
			System.out.println("Wikipedia has " + ctr.get()
					+ " documents parsed in "
					+ (System.currentTimeMillis() - beforeEverythingMillis)
					+ " milliseconds.");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception!", e);
			System.out.println("processed " + ctr.get() + " documents in "
					+ (System.currentTimeMillis() - beforeEverythingMillis)
					+ " milliseconds.");
		}
	}
	
	

}
