package edu.rutgers.kevin.sse;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.james.mime4j.MimeException;
import org.apache.tika.exception.TikaException;
import org.junit.Test;
import org.xml.sax.SAXException;

public class DocumentImplTest {
	private String enronCorpusLocation = "C:\\Work\\SSE\\enron_mail_20110402\\maildir";
	private String shortTestEmailLocation = "martin-t\\discussion_threads\\6";
	private Set<String> shortTestEmailKeywords = new HashSet<String>();

	@Test
	public void testShortEmail() {
		shortTestEmailKeywords.add("to");
		shortTestEmailKeywords.add("discuss");
		shortTestEmailKeywords.add("hpl");
		File shortEmailLocation = new File(enronCorpusLocation,
				shortTestEmailLocation);
		Document shortEmail = new OldEnronDocumentImpl(shortEmailLocation);
		assertEquals(shortEmail.getKeywords(), shortTestEmailKeywords);
	}

	@Test
	public void testEmailTokenize() {
		File emailLocation = new File(
				"/Users/paulgrubbs/data/enron_mail_20110402/maildir/dorland-c/_sent_mail/80.");
		Document shortEmail = new NewEnronDocumentImpl(emailLocation);
		Document oldshortEmail = new OldEnronDocumentImpl(emailLocation);
		System.out.println("New parser:");
		System.out.println(shortEmail.getKeywords());
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("old parser:");
		System.out.println(oldshortEmail.getKeywords());
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("New parser:");
		System.out.println(shortEmail.getKeywordCounts());
	}

	@Test
	public void testMboxParser() {
		File emailLocation = new File(
				"/Users/paulgrubbs/data/test-apache-emails_processed/abdera.apache.org/user/200711");

		try {
			List<String> mails = ApacheEmailDocumentImpl
					.getBodiesOfMBoxEmails(emailLocation);
			for (String s : mails) {
				System.out.println(s);
			}
		} catch (IOException | SAXException | TikaException | MimeException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testMboxKeywordExtraction() {
		File emailLocation = new File(
				"/Users/paulgrubbs/data/test-apache-emails_processed/abdera.apache.org/user/200711");

		Document d = new ApacheEmailDocumentImpl(emailLocation);
		Set<String> kws = d.getKeywords();
		System.out.println(kws);

	}

}
