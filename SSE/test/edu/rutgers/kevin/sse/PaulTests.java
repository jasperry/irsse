package edu.rutgers.kevin.sse;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class PaulTests {
	
	private static class ThreeCombinationGenerator implements Runnable{
		String[] words = null;
		int startIndex = 0;
		int endIndex = 0;
		Set<String> output = null;
		public ThreeCombinationGenerator(String[] words, int startIndex, int endIndex, Set<String> output){
			this.words = words;
			this.startIndex = startIndex;
			this.endIndex = endIndex;
			this.output = output;
		}
		
		@Override
		public void run() {
			for (int i = startIndex; i < endIndex; i++) {
				for (int j = i + 1; j < words.length - 1; j++) {
					for (int k = j + 1; k < words.length; k++) {
						output.add(words[i] + " " + words[j] + " " + words[k]);
					}
				}
			}
			
		}
		
	}
	public static void main(String[] args) {
		
		System.out.println(Math.pow(2, 4));
		String[] list = null;
		String corpus = "Deleting with wildcards can be done using EVAL"
				+ " I suppose this violates the principle of explicitly mentioning all keys affected"
				+ " by the EVAL command but it works and I thinks it is simpler and more "
				+ "efficient than fetching the whole key list to the client and deleting each key individually";
		list = corpus.split(" ");
		Set<String> list_set = new HashSet<String>();
		for(String s : list){
			list_set.add(s);
		}
		
		list = list_set.toArray(new String[0]);
		Set<String> combs = new HashSet<String>();
		Set<String> combs1 = new HashSet<String>();
		System.out.println("list.size: " + list.length);
		// gen 2-combs with nested loops
		for (int i = 0; i < list.length - 1; i++) {
			for (int j = i + 1; j < list.length; j++) {
				combs.add(list[i] + " " + list[j]);
			}
		}
		// System.out.println(combs.toString());
		System.out.println("length: " + combs.size());
		// gen 2-combs with parallel nested loops
		for (int i = 0; i < (list.length - 1) / 2; i++) {
			for (int j = i + 1; j < list.length; j++) {
				combs1.add(list[i] + " " + list[j]);
			}
		}
		for (int i = (list.length - 1) / 2; i < list.length - 1; i++) {
			for (int j = i + 1; j < list.length; j++) {
				combs1.add(list[i] + " " + list[j]);
			}
		}
		// System.out.println(combs1.toString());
		System.out.println("length: " + combs1.size());

		for (String s : combs) {
			boolean contains = combs1.contains(s);
			if (!contains) {
				System.out.println("combs1 does not contain: " + s);
			}
		}
		for (String s : combs1) {
			boolean contains = combs.contains(s);
			if (!contains) {
				System.out.println("combs does not contain: " + s);
			}
		}
		System.out.println("They are the same.");
		System.out.println("Three-word");
		Set<String> three_combs = new HashSet<String>();
		for (int i = 0; i < list.length - 2; i++) {
			for (int j = i + 1; j < list.length - 1; j++) {
				for (int k = j + 1; k < list.length; k++) {
					three_combs.add(list[i] + " " + list[j] + " " + list[k]);
				}
			}
		}
		System.out.println("3-size: " + three_combs.size());
		
		Set<String> three_combs1 = new ConcurrentSkipListSet<String>();
		Thread[] combThreads = new Thread[3];
		combThreads[0] = new Thread(new ThreeCombinationGenerator(list, 0, (list.length - 2)/3, three_combs1));
		combThreads[1] = new Thread(new ThreeCombinationGenerator(list, (list.length - 2)/3, 2*(list.length - 2)/3, three_combs1));
		combThreads[2] = new Thread(new ThreeCombinationGenerator(list, 2*(list.length - 2)/3, (list.length - 2), three_combs1));
		for(Thread t : combThreads){
			t.start();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		for(Thread t : combThreads){
			try {
				t.join();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
		System.out.println("3-size-1: " + three_combs1.size());
		for (String s : three_combs) {
			boolean contains = three_combs1.contains(s);
			if (!contains) {
				System.out.println("three_combs1 does not contain: " + s);
			}
		}
		for (String s : three_combs1) {
			boolean contains = three_combs.contains(s);
			if (!contains) {
				System.out.println("three_combs does not contain: " + s);
			}
		}
	}
}
