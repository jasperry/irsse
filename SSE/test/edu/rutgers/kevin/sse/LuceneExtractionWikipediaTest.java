package edu.rutgers.kevin.sse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Analyzer.TokenStreamComponents;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.FilteringTokenFilter;
import org.apache.lucene.analysis.wikipedia.WikipediaTokenizer;
import org.apache.lucene.util.Version;

import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;

public class LuceneExtractionWikipediaTest {
	static String file = "/Users/paulgrubbs/data/enwiki-20150403-pages-articles.xml";
	static String dir = "/Users/paulgrubbs/data/enwiki/articles/0/foreign_relations_of_iraq";
	static long printAfterThisManyDocuments = 5000;
	static long numProcessed = 0;
	static long beforeTime = System.currentTimeMillis();
	private static final Logger log = Logger
			.getLogger(LuceneExtractionWikipediaTest.class);

	public static void main(String[] args) {
		parseDocumentsAtLocation(new File(dir));
	}

	private static void parseDocumentsAtLocation(File node) {

		if (node.isDirectory()) {
			String[] subNote = node.list();
			for (String filename : subNote) {
				parseDocumentsAtLocation(new File(node, filename));
			}
		} else {
			numProcessed++;
			if (numProcessed % printAfterThisManyDocuments == 0) {
				System.out.println("Processed an additional "
						+ printAfterThisManyDocuments + " documents for "
						+ numProcessed + " total documents in "
						+ ((System.nanoTime() - beforeTime) / 1000000)
						+ " milliseconds. ");
				beforeTime = System.nanoTime();
			}
			Set<String> wikiKeywords = getKeywordsFromDocumentWithAnalyzer(
					node, new LocalWikipediaAnalyzer());
			System.out.println("wiki analyzer:");
			System.out.println(wikiKeywords);
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			Set<String> stdKeywords = getKeywordsFromDocumentWithAnalyzer(node,
					new LocalStandardAnalyzer());
			System.out.println("std analyzer:");
			System.out.println(stdKeywords);
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			SetView<String> diff = Sets.difference(wikiKeywords, stdKeywords);
			System.out.println("diff :");
			System.out.println(diff);
		}

	}

	static class LocalWikipediaAnalyzer extends Analyzer {

		@Override
		protected TokenStreamComponents createComponents(String fieldName,
				Reader reader) {
			WikipediaTokenizer tokenizer = new WikipediaTokenizer(reader);
			LowerCaseFilter filter = new LowerCaseFilter(Version.LUCENE_4_9,
					tokenizer);
			StopFilter sf = new StopFilter(Version.LUCENE_4_9, filter,
					StandardAnalyzer.STOP_WORDS_SET);
			return new TokenStreamComponents(tokenizer, new PorterStemFilter(new NoNumbersFilter(new StandardFilter(
					Version.LUCENE_4_9, sf))));
		}

	}
	static class LocalStandardAnalyzer extends Analyzer {

		@Override
		protected TokenStreamComponents createComponents(String fieldName,
				Reader reader) {
			StandardTokenizer tokenizer = new StandardTokenizer(Version.LUCENE_4_9,reader);
			LowerCaseFilter filter = new LowerCaseFilter(Version.LUCENE_4_9,
					tokenizer);
			StopFilter sf = new StopFilter(Version.LUCENE_4_9, filter,
					StandardAnalyzer.STOP_WORDS_SET);
			return new TokenStreamComponents(tokenizer, new NoNumbersFilter(new StandardFilter(
					Version.LUCENE_4_9, sf)));
		}

	}
	static class NoNumbersFilter extends FilteringTokenFilter{

		public NoNumbersFilter(Version version, TokenStream in) {
			super(version, in);
			
		}
		public NoNumbersFilter(TokenStream in) {
			super(Version.LUCENE_4_9, in);
			
		}
		private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);
		/**
		 * @brief Accept only if the string contains no numbers.
		 */
		@Override
		protected boolean accept() throws IOException {
			char[] c = termAtt.buffer();
			for(int i = 0; i < c.length; i++){
				if(Character.isDigit(c[i]))
					return false;
			}
			return true;
		}	
	}
	/**
	 * @brief Use the Wikipedia tokenizer to extract keywords from a single
	 *        Wikipedia document.
	 */
	private static Set<String> getKeywordsFromDocumentWithAnalyzer(
			File wikipediaDocument, Analyzer az) {
		BufferedReader fr = null;
		Set<String> result = null;

		try {
			fr = new BufferedReader(new FileReader(wikipediaDocument));

			TokenStream stream = null;

			try {
				stream = az.tokenStream(null, fr);
				result = new HashSet<>();
				stream.reset();
				while (stream.incrementToken()) {
					String s = stream.getAttribute(CharTermAttribute.class)
							.toString();
					result.add(s);
				}
				stream.end();
			} finally {
				stream.close();

			}
			return result;
		} catch (IOException e) {
			log.error("Exception!", e);
		} finally {
			try {
				fr.close();
			} catch (IOException e) {
				log.error("Exception!", e);
			}
			az.close();

		}
		return null;
	}

}
