#!/bin/bash

if [ "$2" ]; then
    TFIDF="$1"
    QUERY="$2"
else
    TFIDF="tfidf_all_2001.ser.gz"
    QUERY="$1"
fi


DOCIDS="docID_all.ser.gz"

java -Xmx1024M -cp bin edu.rutgers.kevin.sse.IRSearch $TFIDF $DOCIDS $QUERY

