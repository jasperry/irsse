
package ut;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;


public class UTOptimizer {

    public double[][] encryptedDistance;
    public double[][] plainDistance;
    
    public double[][][] encryptedTriple;
    public double[][][] plainTriple;
    
    public double temperature = 10000.0;
    public double coolingRate = 0.9999;
    public double absoluteTemperature = 0.00001;

    public double Wsingle = 0;
    public double Wpair = 1;
    public double Wtriple = 0;
    
    public boolean normalized = false;
	         
    private ArrayList<Integer> variableList = new ArrayList<>();
    private ArrayList<Integer> domainList = new ArrayList<>();
    
    private HashMap<Integer,Integer> background = new HashMap<>();
    
    private HashMap<Integer,Integer> currentSolution = new HashMap<>();
    private HashMap<Integer,Integer> invertedIndex = new HashMap<>();
	
    private int currVar1, currVar2;
    private int currVal1, currVal2;
    
    
    public UTOptimizer(double initialTemperature, double finalTemperature,
                       double coolingRate){
        
        this.temperature = initialTemperature;
        this.absoluteTemperature = finalTemperature;
        this.coolingRate = coolingRate;
    }
    
    
    public void init(double[][] encryptedDistance, 
                     double[][] plainDistance, 
                     ArrayList<String> knownAssignments,
                     int randSeed){
        
        this.encryptedDistance = encryptedDistance;
        this.plainDistance = plainDistance;
		
        Random generator = new Random(randSeed);
	
        ArrayList<Integer> tempdomainList = new ArrayList<Integer>();
	
        if(knownAssignments != null){
            
            for(int i=0; i< knownAssignments.size(); i++){
                String[] assignment = knownAssignments.get(i).split(":");
                background.put(Integer.parseInt(assignment[0]),
                               Integer.parseInt(assignment[1]));
            }
        }
	
        for(int i=0; i<encryptedDistance.length; i++){
                    
            if(!background.containsKey(i))
                variableList.add(i);
        }
		
        for(int i=0; i<plainDistance.length; i++){
			
            if(!background.containsValue(i)){	
                domainList.add(i);
                tempdomainList.add(i);
            }
        }
	
        for(int i=0; i<encryptedDistance.length; i++){
            
            if(background.containsKey(i))
                currentSolution.put(i, background.get(i));
            else {
                int index = generator.nextInt(tempdomainList.size());
                currentSolution.put(i, tempdomainList.get(index));
                invertedIndex.put(tempdomainList.get(index), i);
                tempdomainList.remove(index);
            }	
        }
    }

    public void changeMode(double Wsingle, double Wpair, double Wtriple, 
                           double[][][] encryptedTriple,
                           double[][][] plainTriple, boolean normalized){
		
        this.encryptedTriple = encryptedTriple;
        this.plainTriple = plainTriple;
        this.normalized = normalized;
		
        this.Wsingle = Wsingle;
        this.Wpair = Wpair;
        this.Wtriple = Wtriple;
    }
	
    public void tuneInitialSolution(double[] encFreq, double[] plainFreq){
		
        ArrayList<IndvFreq> varFreqList = new ArrayList<IndvFreq>();
        ArrayList<IndvFreq> domFreqList = new ArrayList<IndvFreq>();
	
        for(int i=0; i<variableList.size(); i++){
            
            int varInd = (Integer)variableList.get(i);
            double varFreq = encFreq[varInd];
            
            varFreqList.add(new IndvFreq(varInd, varFreq));
        }
	
	
        for(int i=0; i<domainList.size(); i++){
            
            int domInd = domainList.get(i);
            double domFreq = plainFreq[domInd];
            
            domFreqList.add(new IndvFreq(domInd, domFreq));
        }
	
        Collections.sort(varFreqList);
        Collections.sort(domFreqList);
	
        for(int i=0; i<varFreqList.size(); i++){
            
            int varInd = ((IndvFreq)varFreqList.get(i)).index;
            int domInd = ((IndvFreq)domFreqList.get(i)).index;
            
            currentSolution.put(varInd, domInd);
            invertedIndex.put(domInd, varInd);
        }
    }
    
    
    private double evaluateSingle(){
        
        double sum = 0;
        int count=0;
	
        int ind;
	
	
        for(int i=0; i<encryptedDistance.length; i++){
            
            ind = currentSolution.get(i);	
            
            sum += Math.pow((encryptedDistance[i][i] - plainDistance[ind][ind]), 2);
            count++;
        }
	
        if(normalized)
            sum/=count;
	
        return sum;
    }
    
    
    private double evaluatePair(){
        
        double sum = 0;
        int count = 0;
	
        int ind1, ind2;
	
        for(int i=0; i<encryptedDistance.length; i++){
            
            ind1 = currentSolution.get(i);
            
            for(int j= i+1; j<encryptedDistance.length; j++){
                
                ind2 = currentSolution.get(j);
		
                sum += Math.pow((encryptedDistance[i][j] - plainDistance[ind1][ind2]), 2);
                count++;
            }
        }  
	
        if(normalized)
            sum/=count;
	
        return sum;
    }
    
    
    private double evaluateTriple(){
        
        double sum = 0;
        int count=0;
	
        int ind1, ind2, ind3;
	
        for(int i=0; i<encryptedDistance.length; i++){
            
            ind1 = currentSolution.get(i);
            
            for(int j= i+1; j<encryptedDistance.length; j++){
                
                ind2 = currentSolution.get(j);
		
                for(int k= j+1; k < encryptedDistance.length; k++) {
                    
                    ind3 = currentSolution.get(k); 
                    
                    sum += Math.pow((encryptedTriple[i][j][k] -
                                     plainTriple[ind1][ind2][ind3]), 2);
                    count++;
                }
            }
        }  
	
	
        if(normalized)
            sum/=count;
	
        return sum;
    }
    
    
    public double evaluate(){
        
        double result = 0 ;
	
        result += Wsingle*evaluateSingle() + Wpair*evaluatePair();
	
        if (Wtriple > 0)
            result += Wtriple*evaluateTriple();
	
        return result;
    }
    
    
    private void generateNeighbor(){
        
        Random generator = new Random();
	
        currVar1= variableList.get(generator.nextInt(variableList.size()));
        
        currVal1 = currentSolution.get(currVar1);
        currVal2 = domainList.get(generator.nextInt(domainList.size()));
	
        while(currVal1 == currVal2)
            currVal2= domainList.get(generator.nextInt(domainList.size()));
        
        if(currentSolution.containsValue(currVal2))
            currVar2 = invertedIndex.get(currVal2);
        else
            currVar2 = -1;
    }
    
    
    public HashMap<Integer,Integer> anneal(){
        
    	Random generator = new Random();
    	
    	int iteration = 0;
    	boolean change;
    	
    	if(variableList.size() == 1)
    		return currentSolution;
    	
        while (temperature > absoluteTemperature){
        
            double currentCost = evaluate();	
        	
            if(currentCost <= 0.0001)
                break;
        	
            generateNeighbor();
            
            change = true;
            
            currentSolution.put(currVar1, currVal2);
            
            if(currVar2 !=  -1 ){
                currentSolution.put(currVar2, currVal1);
            }
            
            
            double neighborCost = evaluate();
            
            double dist = currentCost - neighborCost;
            
            if(dist <= 0){
            	
            	if( Math.exp(dist / temperature) <= generator.nextDouble()) {
            		
                    change = false;
                    
                    currentSolution.put(currVar1, currVal1);
                    
                    if(currVar2 !=  -1 ){
                        currentSolution.put(currVar2, currVal2);
                    }
            	}
            }

            
            if(change){
            	invertedIndex.put(currVal2, currVar1);
            	invertedIndex.put(currVal1, currVar2);
            }
            
            temperature *= coolingRate;
            if ((iteration & 4095) == 0) {
                System.err.print(temperature + "  ");
            }
            iteration++;
        }
 
       return currentSolution;
    }
}



class IndvFreq implements Comparable<IndvFreq>{
	
	int index;
	double freq;
	
	public IndvFreq(int index, double freq){
		
		this.index = index;
		this.freq = freq;
	}
	
	public int compareTo(IndvFreq arg0) {
	
		if(arg0.freq > this.freq)
			return 1;
		else
			return -1;
	}
}
