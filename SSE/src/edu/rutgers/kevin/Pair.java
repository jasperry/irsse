package edu.rutgers.kevin;


public class Pair<I,J>{
	@Override
	public String toString() {
		String iString="";
		if(i!=null){
			iString=i.toString();
		}
		String jString="";
		if(j!=null){
			jString=j.toString();
		}
		return iString+" : "+jString;
	}
	private I i;
	private J j;
	public Pair(I i,J j){
		this.i=i;
		this.j=j;
	}
	public I first(){
		return this.i;
	}
	public J second(){
		return this.j;
	}
	public boolean equals(Object obj){
		if(!obj.getClass().equals(this.getClass())){
			return false;
		}else{
			@SuppressWarnings("unchecked")
			Pair<I,J> otherPair=(Pair<I,J>)obj;
			if(this.i!=null && otherPair.first()!=null){
				if(this.i.equals(otherPair.first())){
					if(this.j!=null && otherPair.second()!=null){
						return this.j.equals(otherPair.second());
					}else{
						return this.j==null && otherPair.second()==null;
					}
				}else{
					return false;
				}
			}else if(this.i==null && otherPair.first()==null){
				if(this.j!=null && otherPair.second()!=null){
					return this.j.equals(otherPair.second());
				}else{
					return this.j==null && otherPair.second()==null;
				}
			}else{
				return false;
			}
//			return this.i.equals(otherPair.getI()) && this.j.equals(otherPair.getJ());
		}
	}
	@Override
	public int hashCode() {
		int hash = 17 * 31;
		hash = 19 * hash + i.hashCode();
		hash = 19 * hash + j.hashCode();
		return hash;
	}
	
	
}