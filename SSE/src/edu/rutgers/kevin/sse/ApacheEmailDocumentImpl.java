package edu.rutgers.kevin.sse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.MessageBuilder;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.mboxiterator.CharBufferWrapper;
import org.apache.james.mime4j.mboxiterator.MboxIterator;
import org.apache.james.mime4j.message.DefaultMessageBuilder;
import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.xml.sax.SAXException;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

public class ApacheEmailDocumentImpl extends EmailDocumentImpl {
	private static final String APACHE_ENCODING = "ISO-8859-1";
	List<String> allTexts;
	private static final Logger log = Logger
			.getLogger(ApacheEmailDocumentImpl.class);

	public ApacheEmailDocumentImpl(File file) {
		_path = file.getPath();
		f = file;
		az = new EmailAnalyzer();
	}

	public ApacheEmailDocumentImpl(File file, KeywordCount filter) {
		_path = file.getPath();
		this.f = file;
		this.keywordWhitelist = filter;
		az = new EmailAnalyzer(filter.getAllCounts().keySet());
	}

	@Override
	public String getText() {
		lazyBuildConcatenatedString();
		return text;
	}

	private void lazyBuildConcatenatedString() {
		checkText();
		if (text == null) {
			StringBuilder sb = new StringBuilder();
			for (String s : allTexts) {
				sb.append(s);
			}
			this.text = sb.toString();
		}

	}

	private void checkText() {
		if (allTexts == null) {
			try {
				allTexts = getBodiesOfMBoxEmails(f);
			} catch (Exception e) {
				log.error("Cannot get keywords.", e);
				throw new RuntimeException(e);
			}
		}

	}

	@Override
	public Set<String> getKeywords() {
		checkText();
		if (this.keywords != null)
			return this.keywords;
		List<String> kws = new ArrayList<String>();
		try {
			for (String s : allTexts) {
				kws.addAll(tokenizeString(s));
			}

		} catch (IOException e) {
			log.error("Cannot get keywords.", e);
			throw new RuntimeException(e);
		}
		this.keywords = Sets.newHashSet(kws);
		return this.keywords;
	}

	@Override
	public Map<String, Integer> getKeywordCounts() {
		if (keywordCounts != null) {
			return keywordCounts;
		}
		keywordCounts = new HashMap<String, Integer>();
		checkText();

		List<String> kws = new ArrayList<String>();
		try {
			for (String s : allTexts) {
				kws.addAll(tokenizeString(s));
			}
		} catch (IOException e) {
			log.error("Cannot get keywords.", e);
			throw new RuntimeException(e);
		}
		Multiset<String> bag = HashMultiset.create(kws);
		for (String s : bag.elementSet()) {
			keywordCounts.put(s, bag.count(s));
		}
		return keywordCounts;
	}

	// It seems that the unzipped mbox files are ISO-8859, not UTF-8.
	private final static CharsetEncoder ENCODER = Charset.forName(
			APACHE_ENCODING).newEncoder();

	public static List<String> getBodiesOfMBoxEmails(File f)
			throws IOException, SAXException, TikaException, MimeException {
		List<String> bodies = null;

		bodies = new ArrayList<String>();
		MboxIterator mbit = MboxIterator.fromFile(f).charset(ENCODER.charset())
				.build();
		for (CharBufferWrapper message : mbit) {
			List<String> body = getEmailBodies(message.asInputStream(ENCODER
					.charset()));

			bodies.addAll(body);
		}

		return bodies;
	}

	private static List<String> getEmailBodies(InputStream messageBytes)
			throws IOException, MimeException {
		MessageBuilder builder = new DefaultMessageBuilder();
		Message message = builder.parseMessage(messageBytes);
		List<String> retval = new ArrayList<String>();
		recurseThroughEmailBodies(message.getBody(), retval);
		return retval;
	}

	// This method is fucking ugly.
	private static void recurseThroughEmailBodies(Body b,
			List<String> accumulator) {
		if (b instanceof Multipart) {
			Multipart mp = (Multipart) b;
			for (Entity e : mp.getBodyParts()) {
				if (e instanceof Body) {
					recurseThroughEmailBodies((Body) e, accumulator);
				}
			}
		}
		if (b instanceof TextBody) {
			TextBody tb = (TextBody) b;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
				tb.writeTo(baos);
				accumulator
						.add(new String(baos.toByteArray(), APACHE_ENCODING));
			} catch (IOException e) {
				log.error("Cannot get body of email", e);
			}
		} else if (b instanceof BinaryBody) {
			// BinaryBody bb = (BinaryBody) b;
			log.debug("Body part is binary, taking no action");
		} /*
		 * else if (b instanceof SingleBody) { SingleBody sb = (SingleBody) b;
		 * ByteArrayOutputStream baos = new ByteArrayOutputStream(); try {
		 * sb.writeTo(baos); accumulator .add(new String(baos.toByteArray(),
		 * APACHE_ENCODING)); } catch (IOException e) {
		 * log.error("Cannot get body of email", e); } }
		 */

	}

}
