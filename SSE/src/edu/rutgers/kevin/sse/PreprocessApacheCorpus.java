package edu.rutgers.kevin.sse;

import java.io.File;

/**
 * @note This class is meant to be run on the command line. It takes a single
 *       argument, the root of a hierarchy containing gzipped mbox files. It
 *       unzips the files and copies the tree structure to a new directory
 *       called <dirname>_processed.
 * @author paulgrubbs
 * 
 */
public class PreprocessApacheCorpus {

	public static void main(String[] args) {
		ApacheEmailProcessing aep = new ApacheEmailProcessing();
		String root = args[0];
	
		if(root == null){
			System.out.println("You must specify a directory name! Aborting.");
			System.exit(-1);
		}
		File rootFile = new File(root);
		if(!rootFile.exists()){
			System.out.println("You must specify an existing directory! Aborting.");
			System.exit(-1);
		}
		aep.preprocessApacheCorpus(rootFile, "_processed");
	}

}
