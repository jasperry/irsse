
package edu.rutgers.kevin.sse;

import edu.rutgers.kevin.Pair;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author jperry
 */
public class DocRecoveryStats {
    
    CorpusProcessing _dataset;
    
    public DocRecoveryStats(CorpusProcessing dataset) {
        _dataset = dataset;
    }
    
    /** Return recovery rate stats from a single known (public) document. */
    private List<Double> publicKnownDocsOrderedHashes(String filename) {
        File docFile = new File(filename);
        Document publicDoc = new OldEnronDocumentImpl(docFile);
        Set<String> knownKeywords = publicDoc.getKeywords();
        for (String word : knownKeywords) {
            System.out.println(word);
        }
        
        System.out.println("Public doc has " + knownKeywords.size() + " unique keywords.");

        return knownKeywordsRecoveryRate(knownKeywords);
    }
    
    /** Return recovery rate stats from random known docs. */
    private List<Double> randomKnownDocsOrderedHashes(int nDocs, int seed) {
        
        System.out.println("*** Random Known Doc Recovery Stats ***");
        List<Double> stats = new LinkedList<>();
        List<Document> knownDocs = _dataset.getRandomDocs(nDocs, seed);
        System.out.println("Read " + nDocs + " random Documents, seed = " + seed);
        
        // Merge the set of keywords for all the known docs.
        Set<String> allKnownKeywords = new HashSet<>();        
        for (Document doc: knownDocs) {
            allKnownKeywords.addAll(doc.getKeywords());
            // hack to print them all out.
            for (String kw: doc.getKeywords()) {
                System.out.println(kw);
            }
        }
        System.out.println("Recovered " + allKnownKeywords.size() + " unique keywords.");
        
        return knownKeywordsRecoveryRate(allKnownKeywords);        
    }
    
    /** Compute recovery rate stats from a set of known keywords. */
    private List<Double> knownKeywordsRecoveryRate(Set<String> knownKeywords) {
        List<Double> stats = new LinkedList<>();
                double ratioTotal = 0.0;
        int totalKeywordsPerDoc = 0;
        int docCount = 0;
        _dataset.resetDocIter();
        while (_dataset.hasNextDoc()) {
            Document doc = _dataset.nextDoc();
            Set<String> keywords = doc.getKeywords();
            int docSize = doc.numKeywords();
            if (docSize == 0) {
                System.err.println("Whoops! Document with 0 words: " + docCount);
                doc.close();
                continue; // until I debug the iterator.
            }
            docCount++; // get a count of non-empty docs.
            doc.close();
            int knownKeywordsInDoc = 0;
            for (String kw : knownKeywords) {
                if (keywords.contains(kw)) {
                    knownKeywordsInDoc++;
                }
            }
            double knownFrac = (double) knownKeywordsInDoc / docSize;
            stats.add(knownFrac);
            //System.err.print(knownFrac + " ");
            ratioTotal += knownFrac;
            totalKeywordsPerDoc += docSize;
        }
        double averageRecovery = ratioTotal / docCount; //_dataset.numDocs();
        System.out.println("Processed " + docCount + " non-empty docs");
        System.out.println("Average fraction of unique keywords recovered per doc: " +
                           averageRecovery);
        System.out.println("Average keywords per doc: " + (double) totalKeywordsPerDoc
                                                          / docCount);//_dataset.numDocs());
        return stats;
    }
    
    private Pair<Double,Double> chosenUnordered_slices(int numSlices,
            KeywordCount kwCountServer, KeywordCount kwCountClient) {
        // get server KeywordCount from dataset. Is it already ranked?
        List<Pair<String,Integer>> serverkws = kwCountServer.getTopKeywords();
        List<Pair<String,Integer>> clientkws = kwCountClient.getTopKeywords();
        int numRecovered = 0;
        int misses = 0;
        int firstMissIndex = -1;
        
        int binWidth = kwCountServer.size() / numSlices;
        for (int i=0; i < binWidth; i++) {
            List<Integer> clientCounts = new LinkedList<>();
            int ccount_prev = -1; // unused
            // get the next word from each bin. 
            for (int j = 0; j < numSlices; j++) {//(j+i) < kwCountServer.size(); j+= binWidth) {
                String serverWord = serverkws.get(j*binWidth+i).first();
                //System.err.print(serverWord + " " + serverkws.get(j*binWidth+i).second() + ", ");
                int clientCount = kwCountClient.getCount(serverWord);
                if (clientCount != 0) {
                    clientCounts.add(clientCount);
                }
            }
            // Count the number that are out of order.
            int misses_i = countOutOfOrder(clientCounts.toArray(new Integer[0]));
            numRecovered += clientCounts.size() - misses_i;
            misses += misses_i;
        }
        
        int nonPresent = kwCountClient.size() - numRecovered - misses;
        System.out.println(" Recovered " + numRecovered + " keywords out of client's " +
                           kwCountClient.size() + 
                           ", using server's " + kwCountServer.size());
        System.out.println(" Non-present: " + nonPresent + "; Misident: " + misses);
        double recoveryRate = (double) numRecovered / (double) kwCountClient.size();
        System.out.println(" Rate: " + recoveryRate);
        double missRate = (double) misses / (double) kwCountClient.size();
        System.out.println(" Error rate: " + missRate);
        return new Pair<Double,Double>(recoveryRate, missRate);
    }
    
    private static int countOutOfOrder(final Integer[] nums) {
        Integer[] sorted = Arrays.copyOf(nums, nums.length);
        Arrays.sort(sorted, Collections.reverseOrder());
        int outOfOrder = 0;
        boolean onRun = false;
        for (int i=0; i < nums.length; i++) {
            if (nums[i] != sorted[i]) {
                outOfOrder++;
            }
            // Also add repeated counts to misses...
            else if (i > 0 && sorted[i] == sorted[i-1]) {
                if (onRun) { outOfOrder++; }
                else {
                    outOfOrder += 2; 
                    onRun = true;
                }
            }
            else { onRun = false; }; 
        }
        return outOfOrder;
    }
   
    /** Go by pairs, return keyword recovery percentage..OBSOLETED by "slices" */
    /*
    private int chosenUnordered(int numSlices, KeywordCount kwCountServer,
                                  KeywordCount kwCountClient) {
        
        List<Pair<String,Integer>> serverkws;
        List<Pair<String,Integer>> clientkws;
        serverkws = kwCountServer.getTopKeywords();
        clientkws = kwCountClient.getTopKeywords();

        int numRecovered = 0;
        int misses = 0;
        int firstMissIndex = -1;
        int bucketSize = kwCountServer.size() / 2;
        for (int i = 0; i < bucketSize; i++) {
            String servertop = serverkws.get(i).first();
            String serverbot = serverkws.get(i+bucketSize).first();
            System.err.println(servertop + " " + serverkws.get(i).second() + ", " + 
                               serverbot + " " + serverkws.get(i+bucketSize).second());
            // determine if keyword is present in client-indexed data. 
            //  If only one is, it's a freebie!
            int topClientCount = kwCountClient.getCount(servertop);
            int botClientCount = kwCountClient.getCount(serverbot);
            if (topClientCount == 0 && botClientCount == 0) {
                continue;  // neither present, learn nothing
            }
            else if (topClientCount == 0 || botClientCount == 0) {
                numRecovered++; // one present, learn that one.
            }
            // Might be right if they're equal, but let's be conservative.
            else if (topClientCount > botClientCount) {
                numRecovered += 2;  // learn both
            }
            else {
                //System.err.println("  Misident!");
                if (misses == 0) firstMissIndex = i;
                misses += 2;
            }
            
        }
        int nonPresent = kwCountClient.size() - numRecovered - misses;
        System.out.println(" Recovered " + numRecovered + " keywords out of client's " +
                           kwCountClient.size() + 
                           ", using server's " + kwCountServer.size());
        System.out.println(" Non-present: " + nonPresent + "; Misident: " + misses);
        System.out.println(" First miss at position " + firstMissIndex);
        System.out.println(" Rate: " + (double) numRecovered / (double) kwCountClient.size());
        System.out.println(" False pos: " + (double) misses / (double) kwCountClient.size());
        return numRecovered;
    } */ 
    
    /** Return a list of document recovery percentages for a chosen document
        (actually random) with a 50/50 split */
    private List<Double> chosenOrdered(int nDocs, int chosenDocSize) {
    
        System.out.println("****** Chosen doc recovery stats");
        List<Double> stats = new LinkedList<>();
        // retrieve top keywords from training dataset. 
        KeywordCount kwCount = _dataset.getServerKeywordCount();
        List<Pair<String,Integer>> kwTrain = kwCount.getTopKeywords();
        if (chosenDocSize != kwCount.size()) {
            System.err.println("*** Warning: keyword size mismatch: " + 
                               chosenDocSize + " " + kwCount.size());
        }
        System.out.println("Chosen doc with " + chosenDocSize + " keywords.");
        
        // Iterate through client docs. 
        double ratioTotal = 0.0;
        int totalKeywordsPerDoc = 0;
        int docCount = 0;
        _dataset.resetClientDocIter();
        while (_dataset.hasNextClientDoc()) {
            Document doc = _dataset.nextClientDoc();
            Set<String> keywords = doc.getKeywords();
            int docSize = doc.numKeywords();
            if (docSize == 0) {
                System.err.println("Whoops! Document with 0 words after " + docCount + " docs");
                doc.close();
                continue; // until I debug the iterator.
            }
            docCount++; // get a count of non-empty docs.
            doc.close();
            int knownKeywordsInDoc = 0;
            for (Pair<String, Integer> pair : kwTrain) {
                String kw = pair.first();
                if (keywords.contains(kw)) {
                    knownKeywordsInDoc++;
                }
            }
            double knownFrac = (double) knownKeywordsInDoc / docSize;
            stats.add(knownFrac);
            //System.err.print(knownFrac + " ");
            ratioTotal += knownFrac;
            totalKeywordsPerDoc += docSize;
        }
        System.err.println();
        double averageRecovery = ratioTotal / docCount; //_dataset.numDocs();
        System.out.println("Processed " + docCount + " non-empty docs");
        System.out.println("Average fraction of unique keywords recovered per doc: " +
                           averageRecovery);
        System.out.println("Average keywords per doc: " + (double) totalKeywordsPerDoc
                                                          / docCount); //_dataset.numDocs());
        //makeHistogram(stats, 20);
        return stats;
    }
    
    /** Make a histogram of percentage of documents with given percent recovered */
    public static Double[] makeHistogram(List<Double> ratios, int nBuckets) {
        // print out in macine-readable (gnuplot) format.
        Integer[] counts = new Integer[nBuckets];
        Double[] percents = new Double[nBuckets];
        Arrays.fill(counts, new Integer(0));
        for (Double ratio: ratios) {
            int bucket;
            if (ratio == 1.0) bucket = nBuckets - 1;
            else bucket = (int) (ratio * (double) nBuckets);
            counts[bucket]++;
        }
        for (int i = 0; i < nBuckets; i++) {
            percents[i] = (double) counts[i] / ratios.size();
            /* double threshold = (1.0 / (double) nBuckets) * (double) i;
            System.out.printf("(%.2f %d)\n", threshold, percents[i]); */
        }
        return percents;
    }
    
    /** Assumes the histogram is percentages on both axes (1.0/1.0). */
    public static void printHistogram(Double[] hist) {
        int nBuckets = hist.length;
        double mean = 0.0;
        for (int i = 0; i < nBuckets; i++) {
            double threshold = (1.0 / (double) nBuckets) * (double) i;
            System.out.printf("%.2f %.3f\n", threshold, hist[i]);
            mean += threshold * hist[i];
        }
        System.out.println("Histogram mean: " + mean);
    }
    
    public static Double[] hist2CDF(Double[] hist) {
        Double[] result = new Double[hist.length];
        Arrays.fill(result, new Double(0));
        for (int i = 0; i < hist.length; i++) {
            for (int j = i; j < hist.length; j++) {
                result[i] += hist[j];
            }
        }
        return result;
    }
    
    /** Average multiple percentage histograms. */
    public static Double[] averageHistogram(List<Double[]> hists) {
        if (hists.isEmpty()) return null;
        Double[] result = new Double[hists.get(0).length];
        Arrays.fill(result, new Double(0));
        for (Double[] hist : hists) {
            if (hist.length != result.length) {
                System.err.println("ERROR: mismatched histogram size");
                return null;
            }
            for (int i=0; i < result.length; i++) {
                result[i] += hist[i];
            }
        }
        for (int i = 0; i < result.length; i++) {
            result[i] = result[i] / hists.size();
        }
        return result;
    }
    
    public static void main(String[] args) throws FileNotFoundException,
                                                  IOException {
                // experiment parameters, to be read from the properties file.
        int randSeed;
        boolean apacheEmails;
        String apacheDocRoot;
        String enronDocRoot;
        int numKeywords;
        int numRandomDocs;
        boolean doPublicKnownDoc;
        String publicKnownDoc;
        int numChosenDocs;
        int chosenDocSize; // number of words in (the single) chosen doc.
        int numTrials;
        int histBuckets;
        boolean chosenUnordered;
        int numSlices;
        
        // read properties
                Properties props = new Properties();
        String propsfile = "docRecovery.properties";
        /*try (  */
        FileInputStream in_prop = new FileInputStream(propsfile);
        props.load(in_prop);
        in_prop.close();

        System.out.println("***** Keyword Recovery Experiment Parameters *****");
        apacheEmails = Boolean.parseBoolean(props.getProperty("apache_emails"));
        apacheDocRoot = props.getProperty("apache_emails_root").trim();
        if (apacheDocRoot.indexOf("\"") == 0 && apacheDocRoot.lastIndexOf("\"") ==
            apacheDocRoot.length() - 1)
            apacheDocRoot = apacheDocRoot.substring(1, apacheDocRoot.length() - 2);

        enronDocRoot = props.getProperty("doc_root").trim();
        // remove quotes
        if (enronDocRoot.indexOf("\"") == 0 && enronDocRoot.lastIndexOf("\"") ==
            enronDocRoot.length() - 1)
            enronDocRoot = enronDocRoot.substring(1, enronDocRoot.length() - 1); // -2
        randSeed = Integer.parseInt(props.getProperty("rand_seed").trim());
        numKeywords =
            Integer.parseInt(props.getProperty("num_keywords").trim());
        numRandomDocs =
            Integer.parseInt(props.getProperty("num_random_docs").trim());
        doPublicKnownDoc = 
                Boolean.parseBoolean(props.getProperty("do_public_known_doc").trim());
        publicKnownDoc = 
                props.getProperty("public_known_doc").trim();
        if (publicKnownDoc.indexOf("\"") == 0 && publicKnownDoc.lastIndexOf("\"") ==
            publicKnownDoc.length() - 1)
            publicKnownDoc = publicKnownDoc.substring(1, publicKnownDoc.length() - 1);

        numChosenDocs =
            Integer.parseInt(props.getProperty("num_chosen_docs").trim());
        chosenDocSize =
            Integer.parseInt(props.getProperty("chosen_doc_size").trim());
        numTrials = Integer.parseInt(props.getProperty("num_trials").trim());
        histBuckets = Integer.parseInt(props.getProperty("hist_buckets").trim());
        chosenUnordered = 
                Boolean.parseBoolean(props.getProperty("chosen_unordered").trim());
        numSlices = Integer.parseInt(props.getProperty("num_slices").trim());
             
        System.out.println(" doc_root =  \n" + enronDocRoot);
        System.out.println(" apache_emails_root = " + apacheDocRoot);
        System.out.println(" rand_seed =  " + Integer.toString(randSeed));
        System.out.println(" num_keywords =  " + Integer.toString(numKeywords));
        System.out.println(" num_random_docs =  " + Integer.toString(numRandomDocs));
        System.out.println(" do_public_known_doc = " + Boolean.toString(doPublicKnownDoc));
        System.out.println(" public_known_doc = " + enronDocRoot + publicKnownDoc);
        System.out.println(" num_chosen_docs =  " + Integer.toString(numChosenDocs));
        System.out.println(" chosen_doc_size =  " + Integer.toString(chosenDocSize));
        System.out.println(" num_trials = " + Integer.toString(numTrials));
        System.out.println(" hist_buckets = " + Integer.toString(histBuckets));
        System.out.println(" chosen_unordered = " + Boolean.toString(chosenUnordered));
        System.out.println(" num_slices = " + numSlices);
        
        // load and index the Enron dataset for each selected experiment.
        // exper 1 won't need the keywordCount. Maybe Enron shouldn't always do it?
        
        // Run random known doc ordered-hash counts (4.1.1)
        if (numRandomDocs > 0) {
            CorpusProcessing dataset;
            if (apacheEmails) {
                ApacheEmailProcessing apacheDataset = new ApacheEmailProcessing();
                //  This was the unzipping part. 
                // apacheDataset.preprocessApacheCorpus(new File(docRoot), "_proc");
                apacheDataset.noSplitProcessKeywords(numKeywords, new File(apacheDocRoot));
                dataset = apacheDataset;
            }
            else {
                dataset = new EnronProcessing(enronDocRoot, numKeywords, SplitType.NONE,
                                              (float) 1.0, randSeed);
            }
            DocRecoveryStats stats = new DocRecoveryStats(dataset);
            List<Double[]> hists = new LinkedList<>();
            for (int i = 0; i < numTrials; i++) {
                List<Double> ratios = stats.randomKnownDocsOrderedHashes(numRandomDocs, randSeed + i);
                Double[] hist = makeHistogram(ratios, histBuckets);
                hists.add(hist);
                printHistogram(hist);
            }
            Double[] averagedHist = averageHistogram(hists);
            System.out.println("Averaged histogram: ");
            printHistogram(averagedHist);
            System.out.println("Cumulative average histogram:");
            printHistogram(hist2CDF(averagedHist));
        }
        
        // Run publicly-known doc ordered-hash counts (4.1.1b) Only Enron, for now.
        if (doPublicKnownDoc) {
            EnronProcessing dataset = new EnronProcessing(enronDocRoot, numKeywords,
                                                          SplitType.NONE,
                                                          (float) 1.0, randSeed);
            DocRecoveryStats stats = new DocRecoveryStats(dataset);
            List<Double> ratios = 
                    stats.publicKnownDocsOrderedHashes(enronDocRoot + publicKnownDoc);
            Double[] hist = makeHistogram(ratios, histBuckets);
            printHistogram(hist);
        }
        
        // Run chosen doc ordered-hash counts (5.1). Wait, what is this?
        if (numChosenDocs > 0) {
            // Use training-test split dataset
            //  ... BTW, we _do_ want it to compute keyword counts in this case.
            //      We'll only use chosenDocSize words. 
            EnronProcessing dataset;
            List<Double[]> hists = new LinkedList<>();
            for (int i = 0; i < numTrials; i++) {
                // Have to make a new dataset each time to get a new random split.
                dataset = new EnronProcessing(enronDocRoot, chosenDocSize,
                                              SplitType.DISJOINT, (float) 0.5, randSeed+i);
                DocRecoveryStats stats = new DocRecoveryStats(dataset);
                List<Double> ratios = stats.chosenOrdered(numChosenDocs, chosenDocSize);
                Double[] hist = makeHistogram(ratios, histBuckets);
                hists.add(hist);
                printHistogram(hist);
            }
            Double[] averagedHist = averageHistogram(hists);
            System.out.println("Averaged histogram: ");
            printHistogram(averagedHist);
        }
        
        // Run chosen-doc unordered hashes experiment (5.2)
        if (chosenUnordered) {
            boolean apacheOnEnronWords = true;  // when apache = true
            boolean enronOnApacheWords = false; // when apache = false
            // 50% training/indexed split.
            double totalRecoveryRate = 0., totalMissRate = 0.;
            for (int i = 0; i < numTrials; i++) {
                CorpusProcessing dataset;
                if (apacheEmails) {
                    ApacheEmailProcessing apacheData = new ApacheEmailProcessing();
                    apacheData.someSplitProcessKeywords(numKeywords, SplitType.DISJOINT, 
                                               (float) 0.5, randSeed+i, 
                                               new File(apacheDocRoot));
                    dataset = apacheData;
                }
                else {
                    dataset = new EnronProcessing(enronDocRoot, numKeywords,
                                                  SplitType.DISJOINT,
                                                  (float) 0.5, randSeed+i);
                }
                KeywordCount kwCountServer;
                KeywordCount kwCountClient;
                if (apacheOnEnronWords) { // server gets Enron words.
                    EnronProcessing dataset2 = new EnronProcessing(enronDocRoot, numKeywords,
                                                  SplitType.NONE,
                                                  (float) 0.5, randSeed+i);
                    kwCountServer = dataset2.getKeywordCount();
                }
                else if (enronOnApacheWords) {
                    ApacheEmailProcessing dataset2 = new ApacheEmailProcessing();
                    dataset2.noSplitProcessKeywords(numKeywords, new File(apacheDocRoot));
                    kwCountServer = dataset2.getKeywordCount();
                }
                else kwCountServer = dataset.getServerKeywordCount();
                DocRecoveryStats exper = new DocRecoveryStats(dataset);
                kwCountClient = dataset.getClientKeywordCount();
                Pair<Double,Double> rates = exper.chosenUnordered_slices(numSlices,
                        kwCountServer, kwCountClient);
                System.out.println("Chosen Unordered recovery rate: " + rates.first() +
                                   " miss rate: " + rates.second());
                totalRecoveryRate += rates.first(); 
                totalMissRate += rates.second();
            }
            System.out.println("Average recovery rate: " + (totalRecoveryRate / (double) numTrials));
            System.out.println("Average miss rate: " + (totalMissRate / (double) numTrials));
        }
    } // main
    
}
