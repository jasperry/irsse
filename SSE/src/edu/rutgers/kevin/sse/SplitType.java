package edu.rutgers.kevin.sse;

public enum SplitType {
	
		NONE, DISJOINT, SUBSET
	
}
