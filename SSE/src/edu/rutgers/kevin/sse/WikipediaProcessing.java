package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;

import edu.jhu.nlp.wikipedia.*;

public class WikipediaProcessing extends CorpusProcessing {

	private static final Logger log = Logger
			.getLogger(WikipediaProcessing.class);

	public WikipediaProcessing() {

	}

	

	/**
	 * @note This assumes the files have been split from the main XML dump
	 *       already using ArticleWriterPageCallbackHandler or similar.
	 * @note This returns a linked list of every file below 'root'. For
	 *       Wikipedia this is about 3.6 million, so you might want to split it
	 *       up.
	 */
	@Override
	protected LinkedList<File> getAllFiles(File root) {
		LinkedList<File> retval = new LinkedList<>();
		walkSubdirectories(root, new GetAllFilesCallbackHandler(retval, _docIDMap));
		return retval;
	}
	
	
	
	@Override
	protected void getFilesSplit(File root, float serverRatio, Boolean subset) {
		Random rng = new Random(_randSeed);
		GetFilesSplitCallbackHandler gfscallback = new GetFilesSplitCallbackHandler(rng, _docIDMap, serverRatio, subset);
		walkSubdirectories(root, gfscallback);
		_filesServer = gfscallback._fserver;
		_filesClient = gfscallback._fclient;	
	}

	public static void parseWithPageCallback(String dump_file,
			PageCallbackHandler pch) {
		WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser(dump_file);
		long beforeEverythingMillis = 0;

		try {
			long prevMillis = System.currentTimeMillis();
			beforeEverythingMillis = prevMillis;
			wxsp.setPageCallback(pch);

			wxsp.parse();
			log.info("Wikipedia documents parsed in "
					+ (System.currentTimeMillis() - beforeEverythingMillis)
					+ " milliseconds.");
		} catch (Exception e) {

			log.error("Exception!", e);
			log.info("processed documents in "
					+ (System.currentTimeMillis() - beforeEverythingMillis)
					+ " milliseconds.");
		}
	}

	public static void writeFileToLuceneIndex(String title, String content,
			IndexWriter idx) throws IOException {
		if (idx == null)
			throw new RuntimeException("IndexWriter is null.");
		org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
		doc.add(new StringField("title", title, Field.Store.YES));
		doc.add(new TextField("text", content, Field.Store.NO));
		idx.addDocument(doc);
	}

	public static void writeFileToLuceneIndex(String title, Reader content,
			IndexWriter idx) throws IOException {
		if (idx == null)
			throw new RuntimeException("IndexWriter is null.");
		org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
		doc.add(new StringField("title", title, Field.Store.YES));
		doc.add(new TextField("text", content));
		idx.addDocument(doc);
	}
}
