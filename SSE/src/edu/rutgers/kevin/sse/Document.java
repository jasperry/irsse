package edu.rutgers.kevin.sse;

import java.util.Set;
import java.util.Map;

public interface Document {
    public String getPath();
    public String getText();
    public Set<String> getKeywords();
    public Map<String,Integer> getKeywordCounts();
    public int numKeywords();
    public void close();
}
