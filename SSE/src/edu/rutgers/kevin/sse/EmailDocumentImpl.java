package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.EnglishPossessiveFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public abstract class EmailDocumentImpl implements Document {
	protected Set<String> keywords;
	protected String _path;
	// added for tfidf.
	protected Map<String, Integer> keywordCounts;
	protected KeywordCount keywordWhitelist;
	File f;
	String text;
	Analyzer az;
	private static final boolean STEMMING = true;
	@Override
	public int numKeywords() {
		if (keywords != null) {
			return keywords.size();
		} else {
			return 0;
		}
	}

	@Override
	public String getPath() {
		return _path;
	}

	class EmailAnalyzer extends Analyzer {
		Set<String> whitelist;

		public EmailAnalyzer(Set<String> words) {
			this.whitelist = words;
		}

		public EmailAnalyzer() {

		}

		@Override
		protected TokenStreamComponents createComponents(String fieldName,
				Reader reader) {
			StandardTokenizer tokenizer = new StandardTokenizer(
					Version.LUCENE_4_9, reader);
			TokenStream result = new StandardFilter(Version.LUCENE_4_9,
					tokenizer);
			result = new LowerCaseFilter(Version.LUCENE_4_9, result);
			result = new LengthFilter(Version.LUCENE_4_9, result, 3,
					Integer.MAX_VALUE);
			result = new StopFilter(Version.LUCENE_4_9, result,
					StandardAnalyzer.STOP_WORDS_SET);
			result = new EnglishPossessiveFilter(Version.LUCENE_4_9, result);
			if (this.whitelist != null)
				result = new KeywordWhitelistFilter(Version.LUCENE_4_9,
						whitelist, result);
			if (STEMMING) {
				result = new PorterStemFilter(result);
			}
			return new TokenStreamComponents(tokenizer, result);
		}
	}

	public List<String> tokenizeString(String body) throws IOException {
		TokenStream stream = null;
		List<String> result = null;

		try {

			stream = az.tokenStream(null, new StringReader(body));
			result = new ArrayList<>();
			stream.reset();
			while (stream.incrementToken()) {

				String s = stream.getAttribute(CharTermAttribute.class)
						.toString();

				result.add(s);

			}
			stream.end();
		} finally {
			if (stream != null)
				stream.close();

		}
		return result;
	}

	@Override
	public void close() {
		// No-op.
	}

}
