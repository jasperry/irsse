package edu.rutgers.kevin.sse;

import java.util.Map;

public interface ProbabilityMatrix {
    public double[][] getMatrix();
    public double[][] getTopKeywordMatrix(KeywordCount kwcount, int n);
    public int getTopNKeywordRow(String kw);
    public int getTopNRowIndex(int i);
    public Map<String, Integer> getTopNKeywordMap();
    public void addDocument(Document d);
    public double[][] perturbedMatrix(double noise);
    public int size();
    public String getKeywordString(int i);
    
    public int getKeywordIndex(String kw);
    void printMatrix();
}
