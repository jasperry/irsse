package edu.rutgers.kevin.sse;

import java.io.File;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

public class GetFilesSplitCallbackHandler implements FileCallbackHandler {

	public LinkedList<File> _fserver;
	public LinkedList<File> _fclient;
	Random rng;
	Map<Integer, String> docIDMap;
	int numDocs = 0;
	float serverRatio = 0.0f;
	boolean subset;
	public GetFilesSplitCallbackHandler(Random _rng, Map<Integer, String> __docIDMap, float serverRatio, boolean subset){
		this.rng = _rng;
		this._fserver = new LinkedList<File>();
		this._fclient = new LinkedList<File>();
		this.docIDMap = __docIDMap;
		this.serverRatio = serverRatio;
		this.subset = subset;
	}
	@Override
	public void process(File f) {
		 docIDMap.put(numDocs, f.getPath());
		 if (rng.nextFloat() < serverRatio) {
             _fserver.add(f);
             if (subset) // then client searches all files.
             {
                 _fclient.add(f);
             }
         } else {
             _fclient.add(f);
         }
         numDocs++;
	}

}
