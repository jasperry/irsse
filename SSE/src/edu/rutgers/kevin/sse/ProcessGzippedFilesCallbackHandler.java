package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.tika.io.FilenameUtils;

import com.google.common.io.Files;

public class ProcessGzippedFilesCallbackHandler implements FileCallbackHandler {
	private static final Logger log = Logger
			.getLogger(ProcessGzippedFilesCallbackHandler.class);
	String oldPath;
	String suffix;

	public ProcessGzippedFilesCallbackHandler(String originalRootPath,
			String newDirSuffix) {
		this.oldPath = originalRootPath;
		this.suffix = newDirSuffix;
	}

	@Override
	public void process(File f) {
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			if (isGzippedFile(f)) {
				is = new GZIPInputStream(new FileInputStream(f));
				String processedFilePath = getNewPathForFile(
						f.getAbsolutePath(), oldPath, suffix);
				File newFilePath = new File(StringUtils.removeEnd(
						processedFilePath, removeGzipSuffix(f.getName())));
				System.out.println("Writing unzipped file at " + newFilePath);
				newFilePath.mkdirs();
				fos = new FileOutputStream(new File(newFilePath,
						removeGzipSuffix(f.getName())));
				byte[] buffer = new byte[4096];
				int c = 0;
				while ((c = is.read(buffer, 0, 4096)) > 0) {
					fos.write(buffer, 0, c);
				}
			}

		} catch (IOException e) {
			log.error("Exception while trying to write unzipped file", e);
			System.err.println("Couldn't write unzipped file");
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e1) {
					log.error("Exception while trying to close input stream",
							e1);
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					log.error("Exception while trying to close input stream",
							e1);
				}
			}

		}

	}

	/**
	 * @param f
	 * @return
	 */
	private boolean isGzippedFile(File f) {
		return StringUtils.endsWith(f.getAbsolutePath(), ".gz");
	}

	public static String getNewPathForFile(String currPath, String oldRootPath,
			String suffix) throws IOException {
		if (!currPath.startsWith(oldRootPath)) {
			log.error("The file is not in the directory tree under "
					+ oldRootPath + "! Cannot process file.");
			throw new IOException("Cannot process file.");
		}
		String[] pieces = currPath.split(oldRootPath);

		// Normally one checks to make sure the split returned an array with >1
		// elements, but the startsWith check above is (i think) equivalent.
		return oldRootPath + suffix + removeGzipSuffix(pieces[1]);
	}

	public static String removeGzipSuffix(String filename) {
		return StringUtils.removeEnd(filename, ".gz");
	}

	public static void main(String[] args) {
		try {
			String oldPath = "/Volumes/data/apache_emails";
			String suffix = "_processed";
			String filepath = "/Volumes/data/apache_emails/abdera.apache.org/user/200811.gz";
			String newpath = ProcessGzippedFilesCallbackHandler
					.getNewPathForFile(filepath, oldPath, suffix);
			System.out.println("newpath: " + newpath);
			System.out.println(StringUtils.removeEnd(newpath, "200811"));
		} catch (Exception e) {
			System.out.println("Exiting because " + e.getMessage());
			System.exit(-1);
		}
	}

}
