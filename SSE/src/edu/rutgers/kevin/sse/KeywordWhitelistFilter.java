package edu.rutgers.kevin.sse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.FilteringTokenFilter;
import org.apache.lucene.util.Version;

public class KeywordWhitelistFilter extends FilteringTokenFilter {

	Set<String> keywordWhitelist;
	private final CharTermAttribute termAtt = addAttribute(CharTermAttribute.class);

	public KeywordWhitelistFilter(Version version, TokenStream input) {
		super(version, input);

	}

	public KeywordWhitelistFilter(Version version, Set<String> whitelist,
			TokenStream input) {
		super(version, input);
		this.keywordWhitelist = whitelist;
	}

	@Override
	protected boolean accept() throws IOException {
		String s = new String(Arrays.copyOfRange(termAtt.buffer(), 0,
				termAtt.length()));
		boolean retval = keywordWhitelist.contains(s);
		return retval;
	}

}
