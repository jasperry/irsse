package edu.rutgers.kevin.sse;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.rutgers.kevin.Pair;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;

public class KeywordCount {
    
    private File kwFile;
    /** Main data structure: total count per keyword */
    private Map<String,Integer> _counts = new HashMap<String,Integer>();
    /** Number of docs in which keyword has appeared. */
    // private Map<String,Integer> _docCounts = new HashMap<String, Integer>();
    /** Sorted List of Top Keywords */
    private List<Pair<String,Integer>> _sortedCounts;
    /** Indicates whether sortedList needs to be recomputed. */
    private boolean keywordsChanged=true;
    private int numDocs = 0;
    private boolean _keywordFileRead = false;
    
    public KeywordCount(String kwFile) {
        this.kwFile=new File(kwFile);
        // try to read the file.
        if(this.kwFile.exists()) {
            this.readFile();
            keywordsChanged = false;
            System.out.println("Read keywords from " + kwFile);
        } else {
            try {
                this.kwFile.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /** Reads a previously generated sortedList from a file. */
    private void readFile() {
        try {
            _sortedCounts=new ArrayList<Pair<String,Integer>>();
            try (BufferedReader br = new BufferedReader(new FileReader(this.kwFile))) {
                String line;
                line = br.readLine();
                if (line == null) {
                    throw new RuntimeException();
                }
                numDocs = Integer.parseInt(line);
                while((line=br.readLine())!=null) {
                    String[] parts=line.split(":!:");
                    _sortedCounts.add(new Pair<String,Integer>(parts[0],Integer.valueOf(parts[1])));
                    _counts.put(parts[0],Integer.valueOf(parts[1]));
                    // _docCounts.put(parts[0], Integer.valueOf(parts[2]));
                }
            }
        } catch (Exception e) {
            System.err.println("Error reading keyword file " +
                               this.kwFile.getPath());
            throw new RuntimeException(e);
        }
        _keywordFileRead = true;
    }

    /** Tells whether readFile succeeded from the constructor. */
    public Boolean keywordFileRead() { return _keywordFileRead; }

    /** Process a document, adding each keyword and updating docCount. */
    public void addDocument(Document doc) {
        // To make sure we only add to docCount once.
        Set<String> kwAdded = new HashSet<String>();
        
        for(String keyword:doc.getKeywords()) {
            if (keyword != null & !keyword.isEmpty()) {
                this.addKeyword(keyword);   
                /* if (! kwAdded.contains(keyword)) {
                    Integer docCount = _docCounts.get(keyword);
                    if (docCount == null) { docCount = 0; }
                    _docCounts.put(keyword, docCount + 1);
                    kwAdded.add(keyword);
                } */
            }
            else { 
                throw new IllegalArgumentException();
            }
        }
        numDocs++;
    }
    
    /** Add a keyword from a document to the total count. */
    public void addKeyword(String keyword) {
        this.keywordsChanged = true; // update, needs to recalculate top list
        Integer value = _counts.get(keyword);
        if(value == null){
            value = new Integer(1);
        }
        else { value = new Integer(value+1); } // off by one without else!
        _counts.put(keyword,value);
    }
    
    /** Return the sorted list of top keywords, computing if necessary. 
        TODO: maybe a treelist would take care of this better. */
    public List<Pair<String,Integer>> getTopKeywords() {
        if(keywordsChanged) {
            List<Map.Entry<String,Integer>> keyValues =
                new ArrayList<Map.Entry<String,Integer>>(_counts.entrySet());
            Collections.sort(keyValues,
                             new Comparator<Map.Entry<String,Integer>>(){
                                 public int compare(Map.Entry<String,Integer> a,
                                                    Map.Entry<String,Integer> b){
                                     return (-1)*a.getValue().compareTo(b.getValue());
                                 }
                             });
            this._sortedCounts=new ArrayList<Pair<String,Integer>>();
            for(Map.Entry<String,Integer> keyValue:keyValues) {
                _sortedCounts.add(new Pair<String,Integer>(keyValue.getKey(),
                                                        keyValue.getValue()));
            }
            this.keywordsChanged=false;
        }
        return _sortedCounts;
    }
    
    public void writeFile(int startIndex,int endIndex){
        try {
            this.getTopKeywords();  // populates sortedList
            BufferedWriter bw=new BufferedWriter(new FileWriter(this.kwFile));
            bw.write(Integer.toString(numDocs) + "\n");
            for(int i=startIndex; i<=endIndex; i++) {
                Pair<String,Integer> curPair=this._sortedCounts.get(i);
                String output=curPair.first() + ":!:" + curPair.second() + "\n";
                        // ":!:" + _docCounts.get(curPair.getI()) + "\n";
                bw.write(output);
            }
            bw.close();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println("\n  Wrote keywords file " + this.kwFile);
    }
    
    public boolean hasKeyword(String curKeyword) {
        return this._counts.containsKey(curKeyword);
    }
    public Map<String,Integer> getAllCounts(){
    	return this._counts;
    }
    public int size() {
        return this._counts.size();
    }

    public int numDocs() {
        return numDocs;
    }

    public int getCount(String kw) {
        if (_counts.containsKey(kw)) {
            return _counts.get(kw);
        } 
        else return 0;
    }

    /** Frees up memory by repopulating counts and docCounts with only the top 
     * keywords. */
    public void reset(int startIndex,int endIndex) {
        this.getTopKeywords();
        ArrayList<Pair<String,Integer>> tempTopList =
            new ArrayList<Pair<String,Integer>>();
        
        _counts = new HashMap<String,Integer>();
        for(int i=startIndex; i<=endIndex; i++) {
            Pair<String,Integer> curPair=_sortedCounts.get(i);
            tempTopList.add(curPair);
            _counts.put(curPair.first(), curPair.second());
        }
        _sortedCounts=tempTopList;
    }
    
}
