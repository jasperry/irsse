package edu.rutgers.kevin.sse;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

// maybe I should superclass this with a kind of "doc stats" so
//  that ProbMatrix and TfIdf all have a common interface.

/** Generate and access an inverted keyword index,
    by ingesting DocumentImpl instances. */
public class InvertedIndex implements java.io.Serializable {
    int[][] _matrix = null;
    transient KeywordCount _keywordSet; // List of considered keywords
    // Construct our own mapping of keywords to rows as we see them. 
    Map<String,Integer> _kwMap = new HashMap<String,Integer>();
    // When needed: mappings from columns to doc links.
    //  ?? Or should I leave it for EnronProcessing?
    // Map<Integer, String> _docLinkMap = new HashMap<Integer,String>();
    int _docCount = 0;
    int _kwCount = 0;
    int _totalEntries = 0;
    int _paddingSize = 0; 
    int _paddingMult = 0;
    int _numPaddingDocs = 0;
    int _paddingStartCol = 0;

    // To simulate attack: Attacker sees queried set of rows (submatrix?)
    // Doesn't know what number they are.

    /** Allocate the term/document matrix */
    public InvertedIndex(KeywordCount keywords, int paddingMult) {
        _keywordSet = keywords;
        _paddingMult = paddingMult;
        System.out.println("Initializing Inverted Index matrix for " +
                           Integer.toString(_keywordSet.size()) + " keywords, " +
                           Integer.toString(_keywordSet.numDocs()) + " docs");
        
        // Compute number of fake documents for padding, if needed.
        _paddingStartCol = _keywordSet.numDocs(); // same for padding or not.
        if (paddingMult > 0) {
            // Here we get a preliminary estimate of the # of keywords per document. 
            //  I did a log fit to the keywords-docsize ratio. This allows us to 
            //  allocate the index columns for padding at the start. 
            double keywordsPerDoc = 7.91642 * Math.log(0.0301093 * _keywordSet.size());
            _numPaddingDocs = (int) ((paddingMult / 2 * _keywordSet.size()) / keywordsPerDoc);
            System.out.println("Will pad index with " + _numPaddingDocs + " fake docs");
        }
        _matrix = new int[_keywordSet.size()][_keywordSet.numDocs() + _numPaddingDocs];
      
    }

    public void addDocument(Document doc) {
        
        for(Map.Entry<String,Integer> entry : doc.getKeywordCounts().entrySet()) {
            String word = entry.getKey();
            // We don't care about counts, just whether the word exists.
            if (_keywordSet.hasKeyword(word)) {
                Integer row = _kwMap.get(word);
                if (row == null) {
                    row = _kwCount; _kwMap.put(word, _kwCount);  _kwCount++;
                }
                _matrix[row][_docCount] = 1;
                _totalEntries++;
                // Find the row it's in or make a new one.
            }
        }
        // _docLinkMap.add(_docCount, doc.getPath());
        _docCount++;
    }

    /** Call after all documents are added, to pad up to the threshold. */
    public void addPadding() { 
        System.out.println("Padding term-doc matrix to nearest " + _paddingMult);
        Random rng = new Random(999); // fixed for now
        for (int i=0; i < _matrix.length; i++) {
            int mod = this.getRowCount(i) % _paddingMult;
            if (mod == 0) continue;
            for (int k = 0; k < _paddingMult - mod; k++) {
                int col;
                do {
                    col = rng.nextInt(_numPaddingDocs) + _paddingStartCol;
                } while (_matrix[i][col] != 0); // no repeats.
                _matrix[i][col] = 1;
                _paddingSize++;
            }
            //System.out.print(" " + this.getRowCount(i)); // to check.
        }
        // Now, lie about the document count.
        _docCount += _numPaddingDocs;
    }
    
    /** Return the total number of index entries, not counting padding. */
    public int indexSize() { return _totalEntries; }
    
    /** Return the total number of entries in the padding. */
    public int paddingSize() { return _paddingSize; }
    
    
    public int numDocs() { return _docCount; }

    public int numKeywords() { return _kwCount; }

    /** Return rows specified by a list of ints */
    /* public int[][] getRows(Vector<Integer> rowInds) {
        return null; 
    } */

    /** Return a single row */
    public int[] getRow(int r) {
        return _matrix[r];
    }
    /** Return the number of nonzero entries in a row */
    public int getRowCount(int r) {
        int count = 0;
        for (int i=0; i < _matrix[r].length; i++) {
            if (_matrix[r][i] != 0) count++;
        }
        return count;
    }
    
    /** Return nonzero entries in a row, skipping columns in a sorted skip list. */
    public int getRowCountSkipList(int r, Integer[] skipList) {
        int count = 0;
        int skipIdx = 0;
        for (int i=0; i < _matrix[r].length; i++) {
            if (skipIdx < skipList.length && skipList[skipIdx] == i) skipIdx++;
            else if (_matrix[r][i] != 0) count++;
        }
        return count;
    }
    
    public int getRowCountNoPadding(int r) {
        int count = 0;
        for (int i=0; i < _paddingStartCol; i++) {
            if (_matrix[r][i] != 0) count++;
        }
        return count;
    }

    /** Get all rows with a given count. */
    /*public Vector<Integer> getWordsWithCount(int count) {
        
      }*/



} // class InvertedIndex
