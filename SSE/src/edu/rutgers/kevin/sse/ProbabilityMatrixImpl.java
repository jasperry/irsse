package edu.rutgers.kevin.sse;

import edu.rutgers.kevin.Pair;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class ProbabilityMatrixImpl implements ProbabilityMatrix {
    private Map<String,Integer> keywordMap=new HashMap<>();
    /** To save the keyword-row correspondence for the top-N matrix. */
    private Map<String, Integer> _topNKeywordMap = new HashMap<>();
    /** To save the row-row correspondence of the top-N matrix. */
    private Map<Integer, Integer> _topNRowMap = new HashMap<>();
    private ArrayList<String> inverseKwMap = null;
    private double[][] rawMatrix;
    private double[][] probMatrix;
    private boolean matrixUpToDate=false; // true;
    int maxKeywordNumber=-1;
    int numDocuments=0;
    
    public ProbabilityMatrixImpl(int size) {
        this.rawMatrix=new double[size][size];
        this.probMatrix=new double[size][size];
        inverseKwMap = new ArrayList<String>(size);
    }
    
    @Override
    public double[][] getMatrix() {
        if(matrixUpToDate){
            return probMatrix;
        } else { // recompute probabilities from raw counts.
            probMatrix=new double[rawMatrix.length][rawMatrix.length];
            for(int i=0; i<rawMatrix.length; i++){
                for(int j=0; j<rawMatrix.length; j++){
                    probMatrix[i][j]=rawMatrix[i][j]/numDocuments;
                }
            }
        }
        System.out.println("    Returning matrix with " +
                           Integer.toString(probMatrix.length) + " rows");
                           //Integer.toString(numDocuments) + " documents");
        return probMatrix;
    }
    
    /** Return submatrix of only top n keywords, setting the map. */
    public double[][] getTopKeywordMatrix(KeywordCount kwcount, int n) {
        if (! matrixUpToDate) {
            this.getMatrix();
        }
        double[][] topMatrix = new double[n][n];
        List<Pair<String, Integer>> topList = kwcount.getTopKeywords();
        for (int i=0; i < n; i++) {
            String kw = topList.get(i).first();
            int rowindex = this.getKeywordIndex(kw);
            // save the keyword correspondence to its row IN THE NEW MATRIX.
            _topNKeywordMap.put(kw, i);
            _topNRowMap.put(i, rowindex);
            for (int j = 0; j < n; j++) {
                topMatrix[i][j] = probMatrix[rowindex]
                                            [this.getKeywordIndex(topList.get(j).first())];
            }
        }
        return topMatrix;
    }
    
    @Override
    public int getTopNKeywordRow(String kw) {
        return _topNKeywordMap.get(kw);
    }
    
    @Override
    public Map<String, Integer> getTopNKeywordMap() {
        return _topNKeywordMap;
    } 

    @Override
    public int getTopNRowIndex(int i) {
        return _topNRowMap.get(i);
    }
    
    
    @Override
    public int size() {
        return rawMatrix.length;
    }

    /** Add the keywords from a document to the co-occurrence matrix */
    @Override
    public void addDocument(Document d) {
        matrixUpToDate=false; 
        for(String firstKeyword:d.getKeywords()) {
            for(String secondKeyword:d.getKeywords()) {
                if(!firstKeyword.equals(secondKeyword)) {
                    Integer firstKeywordMapping=keywordMap.get(firstKeyword);
                    if(firstKeywordMapping==null) {
                        firstKeywordMapping=(++maxKeywordNumber);
                        keywordMap.put(firstKeyword, firstKeywordMapping);
                        inverseKwMap.add(maxKeywordNumber, firstKeyword);
                    }
                    Integer secondKeywordMapping=keywordMap.get(secondKeyword);
                    if(secondKeywordMapping==null) {
                        secondKeywordMapping=(++maxKeywordNumber);
                        keywordMap.put(secondKeyword,secondKeywordMapping);
                        inverseKwMap.add(maxKeywordNumber, secondKeyword);
                    }
                    if(maxKeywordNumber>=rawMatrix.length) {
                        doubleMatrix();
                    }
                    rawMatrix[firstKeywordMapping][secondKeywordMapping] =
                        rawMatrix[firstKeywordMapping][secondKeywordMapping]+1;
                    rawMatrix[secondKeywordMapping][firstKeywordMapping] =
                        rawMatrix[secondKeywordMapping][firstKeywordMapping]+1;
                }
            }
        }
        numDocuments++;
    }

    @Override
    public String getKeywordString(int i) { return inverseKwMap.get(i); }
    
    @Override
    public int getKeywordIndex(String kw) { return keywordMap.get(kw); }
    
    /** I have no idea what this is for. */
    private void doubleMatrix() {
        //printMatrix();
        System.out.println(numDocuments);
        double[][] newMatrix=new double[rawMatrix.length*2][rawMatrix.length*2];
        for(int i=0;i<rawMatrix.length;i++) {
            for(int j=0;j<rawMatrix.length;j++) {
                newMatrix[i][j]=rawMatrix[i][j];
            }
        }
        rawMatrix=newMatrix;
    }

    @Override
    public void printMatrix() {
        getMatrix(); // refreshes probabilities.
        for (int i = 0; i < probMatrix.length; i++) {
            //double rowSum=0;
            for (int j = 0; j < probMatrix.length; j++) {
                System.out.print(Double.toString(probMatrix[i][j]) + ", ");
            }
            System.out.println();
        }
    }

    @Override
    public double[][] perturbedMatrix(double noise) {
        throw new UnsupportedOperationException();
    }

}
