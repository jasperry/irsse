package edu.rutgers.kevin.sse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import porter.Stemmer;

public class OldEnronDocumentImpl implements Document {
    private KeywordCount keywordWhitelist;
    private BufferedReader br;
    private StringBuilder text=new StringBuilder();
    private StringTokenizer tokenizer=null;
    private Set<String> keywords=null;
    private String _path;
    // added for tfidf.
    private Map<String,Integer> keywordCounts = null; 
    private Pattern metadataPattern=Pattern.compile("(Message-ID:|Date:|From:|To:|Subject:|Cc:|Mime-Version:|Content-Type:"
                                                    + "|Content-Transfer-Encoding:|Bcc:|X-From:|X-To:|X-cc:|X-bcc:|X-Folder:|X-Origin:|X-FileName:|cc:).*");
    private static final String DELIMS=" \t\n\r\f.,?-!";

    public OldEnronDocumentImpl(File file) {
        _path = file.getPath();
        try {
            this.br=new BufferedReader(new FileReader(file));
        } catch(IOException e) {
            System.err.println("!!! Failed to read file: " + file.getPath());
            throw new RuntimeException();
        }
    }
    public OldEnronDocumentImpl(File file,KeywordCount filter) {
        _path = file.getPath();
        try{
            this.br=new BufferedReader(new FileReader(file));
            this.keywordWhitelist=filter;
        }catch(Exception e){
            System.err.println("File read failed: " + file.getPath());
            throw new RuntimeException();
        }
    }

    public String getPath() { return _path; }
    
    private void processFileSkipMetadata(){
        if(this.tokenizer==null){
            try{
                String line;
                //while((line=this.br.readLine())!=null){
                //	if(!this.metadataPattern.matcher(line).matches()){
                //		break;
                //	}
                //}
                //this.text.append(line+"\n");
                while((line=this.br.readLine())!=null){
                    if(!this.metadataPattern.matcher(line).matches()){
                        this.text.append(line+"\n");
                    }
                }
                this.tokenizer=new StringTokenizer(text.toString(),OldEnronDocumentImpl.DELIMS);
            }catch(IOException e){
                throw new RuntimeException(e);
            }
        }
    }

    public String getText() { return this.text.toString(); }
    
    @Override
    public Set<String> getKeywords() {
        if(this.tokenizer==null) {
            this.processFileSkipMetadata();
        }
        if(this.keywords!=null){
            return this.keywords;
        }
        this.keywords=new HashSet<String>();
        Stemmer stemmer=new Stemmer();
        String token;
        while(this.tokenizer.hasMoreTokens()){
            token=this.tokenizer.nextToken();
            //Probably possible to not make a new one every loop.
            stemmer.add(token.toLowerCase().toCharArray(),token.length());
            stemmer.stem();
            String curKeyword=stemmer.toString();
            if(this.keywordWhitelist!=null){
                if(this.keywordWhitelist.hasKeyword(curKeyword)) {
                    this.keywords.add(curKeyword);
                }
            } else {
                this.keywords.add(curKeyword);
            }
        }
        return this.keywords;
    }
    
    @Override
    public Map<String,Integer> getKeywordCounts() {
        if (keywordCounts != null) {
            return keywordCounts;
        }
        keywordCounts = new HashMap<String,Integer>();
        if(this.tokenizer==null) {
            this.processFileSkipMetadata();
        }
        String token;
        Stemmer stemmer = new Stemmer();
        while(this.tokenizer.hasMoreTokens()) {
            token=this.tokenizer.nextToken();
            stemmer.add(token.toLowerCase().toCharArray(),token.length());
            stemmer.stem();
            String word=stemmer.toString();
            if(this.keywordWhitelist == null || this.keywordWhitelist.hasKeyword(word)) {
                    int count = keywordCounts.containsKey(word) ?
                        keywordCounts.get(word) : 0;
                    keywordCounts.put(word, count + 1);
            }
        }
        return keywordCounts;
    }

    @Override
    public int numKeywords() {
        return keywords.size();
    }
    
    @Override
    public void close() {
        try {
            br.close();
        } catch(IOException e){
            //System.err.println("!!! Failed to close file.");
            throw new RuntimeException();
        }
        
    }

}
