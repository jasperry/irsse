package edu.rutgers.kevin.sse;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class ThreeCombinationGenerator implements Runnable {
	InvertedIndex invIdx = null;
	int startIndex = 0;
	int endIndex = 0;
	Set<Integer> threeWordDocs = null;
	ArrayList<Integer> nonTwoWordDocs = null;
	int threadNum = 0;
	AtomicLong counter = null;
	public ThreeCombinationGenerator(InvertedIndex invIdx, int startIndex,
			int endIndex, Set<Integer> threeWordDocs,
			ArrayList<Integer> nonTwoWordDocs, int threadNum, AtomicLong totalCTR) {
		this.invIdx = invIdx;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.threeWordDocs = threeWordDocs;
		this.nonTwoWordDocs = nonTwoWordDocs;
		this.threadNum = threadNum;
		this.counter = totalCTR;
	}

	@Override
	public void run() {
		int f = 0;
		int total = 0;
		long prev = System.currentTimeMillis();
		for (int i = startIndex; i < endIndex; i++) {
			for (int j = i + 1; j < invIdx.numKeywords() - 1; j++) {
				for (int k = j + 1; k < invIdx.numKeywords(); k++) {
					total++;
					int[] row1 = invIdx.getRow(i);
					int[] row2 = invIdx.getRow(j);
					int[] row3 = invIdx.getRow(k);
					int numMatchingDocs = 0;
					int candidateDoc = -1; // overwritten if match, so no
											// matter
					for (int inner_j : nonTwoWordDocs) {
						if (row1[inner_j] > 0 && row2[inner_j] > 0
								&& row3[inner_j] > 0) {
							numMatchingDocs++;
							candidateDoc = inner_j;
						}
					}
					if (numMatchingDocs == 1) {
						threeWordDocs.add(candidateDoc);
					}
				}
			}
			f++;
			if (f % 250 == 0) {
				System.out.println("Thread " + threadNum
						+ " has processed an additional 250 keywords in "
						+ (System.currentTimeMillis() - prev) + " millis.");
				prev = System.currentTimeMillis();
			}
		}
		System.out.println("Thread " + threadNum + "  processed " + total
				+ " combinations total.");
		counter.addAndGet(total);

	}


}
