package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Random;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

public class ApacheEmailProcessing extends CorpusProcessing {
	private static final Logger log = Logger
			.getLogger(ApacheEmailProcessing.class);

	@Override
	protected void getFilesSplit(File root, float serverRatio, Boolean subset) {
		Random rng = new Random(_randSeed);
		GetFilesSplitCallbackHandler gfscallback = new GetFilesSplitCallbackHandler(
				rng, _docIDMap, serverRatio, subset);
		walkSubdirectories(root, gfscallback);
		_filesServer = gfscallback._fserver;
		_filesClient = gfscallback._fclient;
	}

	@Override
	protected LinkedList<File> getAllFiles(File root) {
		LinkedList<File> retval = new LinkedList<>();
		walkSubdirectories(root, new GetAllFilesCallbackHandler(retval,
				_docIDMap));
		return retval;
	}

	public void preprocessApacheCorpus(File root, String newDirSuffix) {
		System.out.println("Preprocessing Apache from " + root.getAbsolutePath());
		walkSubdirectories(root,
				new ProcessGzippedFilesCallbackHandler(root.getAbsolutePath(),
						newDirSuffix));
	}
        
	public void noSplitProcessKeywords(int nKeywords, File docRoot) {
		// get files
		_filesAll = getAllFiles(docRoot);
		System.out.println("Found " + Integer.toString(_filesAll.size())
				+ " email files");
		_filesAllIter = _filesAll.iterator();

		// keyword count - should have a parameter so don't always construct it?
		String keywordFile = "kw_apache_" + Integer.toString(nKeywords)
		                     + "_all.txt";
		// KeywordCount constructor will try to read the file.
		_kwCountAll = new KeywordCount(keywordFile);
		if (!_kwCountAll.keywordFileRead()) {
			storeKeywordList(_kwCountAll, _filesAll, NSTOPWORDS, NSTOPWORDS
					+ nKeywords);
		}
	}
        
        // The only difference with Enron is the filename strings...
        public void someSplitProcessKeywords(int nKeywords, SplitType splitType, 
                                               float serverRatio, int randSeed, 
                                               File docRoot) {
            _randSeed = randSeed;
            getFilesSplit(docRoot, serverRatio, (splitType == SplitType.SUBSET));
            System.out.println("File split: Server: "
				+ Integer.toString(_filesServer.size()) + " Client: "
				+ Integer.toString(_filesClient.size()));

            // get top keyword counts for training (server) set
            String kwFileServer = "kw_apache" + Integer.toString(nKeywords) + "_s"
				+ Integer.toString(randSeed) + "_"
				+ String.format("%.2f", serverRatio) + "frac_srv.txt";
            _kwCountServer = new KeywordCount(kwFileServer);
            if (!_kwCountServer.keywordFileRead()) {
                System.out.println("Generating server keyword list of size "
                                   + Integer.toString(nKeywords));
                storeKeywordList(_kwCountServer, _filesServer, NSTOPWORDS,
		    		 NSTOPWORDS + nKeywords);
            }

            // get top keyword counts for real (client) set
            String kwFileClient = "kw_apache" + Integer.toString(nKeywords)
				+ "_s" + Integer.toString(randSeed)
				+ "_" + String.format("%.2f",
					(splitType == SplitType.DISJOINT) ? (1.0 - serverRatio)
					 : 1.0) + "frac_cli.txt";
            _kwCountClient = new KeywordCount(kwFileClient);
            if (!_kwCountClient.keywordFileRead()) {
                System.out.println("Generating client keyword list of size "
                                   + Integer.toString(nKeywords));
                storeKeywordList(_kwCountClient, _filesClient, NSTOPWORDS,
                                 NSTOPWORDS + nKeywords);
            }
            _filesClientIter = _filesClient.iterator();
	}



}
