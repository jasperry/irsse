package edu.rutgers.kevin.sse;

import java.io.File;

public interface FileCallbackHandler {
	public void process(File f);
}
