package edu.rutgers.kevin.sse;

import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicLong;

/** For each document, find the set of keywords that uniquely identifies. */
class UniqueKeywords {
	static int num_threads = 8;

	public static void main(String[] args) throws FileNotFoundException,
			IOException {

		// experiment parameters, to be read from the properties file.
		// int randSeed;
		String docRoot;
		int numKeywords;

		boolean writeMatrices = false;
		boolean testTriples = true;

		// read properties
		Properties props = new Properties();
		String propsfile = "countAttack.properties";
		/* try ( */
		FileInputStream in_prop = new FileInputStream(propsfile);
		props.load(in_prop);
		in_prop.close();

		System.out.println("***** Unique Keyword Experiment Parameters *****");
		docRoot = props.getProperty("doc_root").trim();
		// remove quotes
		if (docRoot.indexOf("\"") == 0
				&& docRoot.lastIndexOf("\"") == docRoot.length() - 1)
			docRoot = docRoot.substring(1, docRoot.length() - 2);
		// randSeed = Integer.parseInt(props.getProperty("rand_seed").trim());
		numKeywords = Integer
				.parseInt(props.getProperty("num_keywords").trim());
		// queryPercent =
		// Integer.parseInt(props.getProperty("query_percent").trim());
		System.out.println(" doc_root =  \n" + docRoot);
		// System.out.println(" rand_seed =  " +
		// Integer.toString(randSeed));
		System.out.println(" num_keywords =  " + Integer.toString(numKeywords));

		// load and index the Enron dataset
		EnronProcessing enron = new EnronProcessing(docRoot, numKeywords,
				SplitType.NONE, (float) 1.0, 42); // randseed n/a
		InvertedIndex invIdx = enron.genInvertedIndex(0);

		// start with unique. I can do this just by looking at the rows.
		// depends on # of keywords considered.
		int numUniq = 0;
		for (int i = 0; i < invIdx.numKeywords(); i++) {
			int[] row = invIdx.getRow(i);
			int nonzero = 0;
			for (int j = 0; j < row.length; j++) {
				if (row[j] > 0)
					nonzero++;
				if (nonzero > 2)
					break;
			}
			if (nonzero == 1)
				numUniq++;
			if (i % 2500 == 0)
				System.out.println("Processed an additional 2500 documents.");
		}
		System.out.println("Number of one-word identifying documents: "
				+ numUniq);

		Set<Integer> twoWordDocs = twoWordTest(invIdx);

		// now the three test
		if (!testTriples) {
			return;
		}
		Set<Integer> threeWordDocs = threeWordTest(invIdx, twoWordDocs);
		
		
		int numIdentified = twoWordDocs.size() + threeWordDocs.size();
		System.out.println("Number of three-word identifying documents: "
				+ threeWordDocs.size() + ",\n two-and-three combined: "
				+ numIdentified + " (" + numIdentified
				/ ((double) invIdx.numDocs()) + ")");

	} // main

	/**
	 * @param invIdx
	 * @param twoWordDocs
	 * @return
	 */
	private static Set<Integer> threeWordTest(InvertedIndex invIdx,
			Set<Integer> twoWordDocs) {

		// make the list of all the columns we care about.
		// Should be faster than testing every time.
		ArrayList<Integer> nonTwoWordDocs = new ArrayList<>();
		for (int i = 0; i < invIdx.numDocs(); i++) {
			if (!twoWordDocs.contains(i)) {
				nonTwoWordDocs.add(i);
			}
		}

		List<Set<Integer>> threeWordDocSets = new ArrayList<>();
		System.out.println("Processing three-word combinations.");
		
		
		AtomicLong three_ctr = new AtomicLong(0);
		List<Thread> combThreads = createThreeWordTasksBalancedLoad(threeWordDocSets, invIdx, three_ctr, nonTwoWordDocs);
		long prev = System.currentTimeMillis();
		for (Thread t : combThreads) {
			t.start();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		for (Thread t : combThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

		System.out.println("Time taken, millis: "
				+ (System.currentTimeMillis() - prev) + " Checked "
				+ three_ctr.get() + " combinations.");
		Set<Integer> threeWordDocs = new HashSet<Integer>();
		for (Set<Integer> e : threeWordDocSets) {
			threeWordDocs.addAll(e);
		}
		return threeWordDocs;
	}
	private static List<Thread> createThreeWordTasksBalancedLoad(List<Set<Integer>> threeWordDocSets, InvertedIndex invIdx,
			AtomicLong three_ctr, ArrayList<Integer> nonTwoWordDocs){
		int prev_end = 0;
		List<Thread> combThreads = new ArrayList<Thread>();
		for (int v = 1; v <= num_threads; v++) {
			Set<Integer> vthSet = new HashSet<Integer>();
			threeWordDocSets.add(vthSet);
			int curr_end = (int) ((invIdx.numKeywords() - 2) * (1 / ((Math.pow(
					1.4, num_threads - v)))));
			System.out.println("currStart: " + prev_end + " currEnd: "
					+ curr_end + " for thread " + v);

			combThreads.add(new Thread(new ThreeCombinationGenerator(invIdx,
					prev_end, curr_end, vthSet, nonTwoWordDocs, v, three_ctr)));

			prev_end = curr_end;
		}
		return combThreads;
	}
	private static List<Thread> createTasksBalancedLoad(
			List<Set<Integer>> twoWordDocSets, InvertedIndex invIdx,
			AtomicLong ctr) {
		List<Thread> twoCombThreads = new ArrayList<Thread>();
		int prev1 = 0;
		for (int x = 1; x <= num_threads; x++) {
			Set<Integer> xthSet = new HashSet<Integer>();
			twoWordDocSets.add(xthSet);
			int currStartIndex = prev1;
			int currEndIndex = (int) ((invIdx.numKeywords() - 1) * (1 / ((Math
					.pow(1.4, num_threads - x)))));
			System.out.println("currStart: " + currStartIndex + " currEnd: "
					+ currEndIndex + " for thread " + x);
			twoCombThreads.add(new Thread(new TwoCombinationGenerator(invIdx,
					currStartIndex, currEndIndex, xthSet, x, ctr)));
			prev1 = currEndIndex;
		}
		return twoCombThreads;
	}

	@SuppressWarnings("unused")
	private static List<Thread> createTasksUniformLoad(
			List<Set<Integer>> twoWordDocSets, InvertedIndex invIdx,
			AtomicLong ctr) {
		List<Thread> twoCombThreads = new ArrayList<Thread>();
		int prev_start_two = 0;
		for (int x = 0; x < num_threads; x++) {
			Set<Integer> xthSet = new HashSet<Integer>();
			twoWordDocSets.add(xthSet);
			System.out.println("prev_start: " + (x)
					+ "*(invIdx.numKeywords()-1)/" + num_threads
					+ " curr_end: " + (x + 1) + "*(invIdx.numKeywords()-1)/"
					+ num_threads);
			int curr_end = (x + 1) * (invIdx.numKeywords() - 1) / num_threads;
			twoCombThreads.add(new Thread(new TwoCombinationGenerator(invIdx,
					prev_start_two, curr_end, xthSet, x, ctr)));
			prev_start_two = curr_end;

		}
		return twoCombThreads;
	}

	/**
	 * @param invIdx
	 * @return
	 */
	private static Set<Integer> twoWordTest(InvertedIndex invIdx) {
		List<Set<Integer>> twoWordDocSets = new ArrayList<>();

		AtomicLong ctr = new AtomicLong();
		
		
		List<Thread> twoCombThreads = createTasksBalancedLoad(twoWordDocSets, invIdx, ctr);

		long prev = System.currentTimeMillis();
		for (Thread t : twoCombThreads) {
			t.start();
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		for (Thread t : twoCombThreads) {
			try {
				t.join();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		System.out.println("Time taken, millis: "
				+ (System.currentTimeMillis() - prev) + " Checked " + ctr.get()
				+ " combinations.");

		Set<Integer> twoWordDocs = new HashSet<Integer>();

		for (Set<Integer> e : twoWordDocSets) {
			twoWordDocs.addAll(e);
		}
		System.out
				.println("Number of two-word identifying documents from new method: "
						+ twoWordDocs.size()
						+ " ("
						+ twoWordDocs.size()
						/ ((double) invIdx.numDocs()) + ")");

		return twoWordDocs;
	}

}
