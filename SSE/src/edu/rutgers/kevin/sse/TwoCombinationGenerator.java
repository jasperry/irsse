package edu.rutgers.kevin.sse;

import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class TwoCombinationGenerator implements Runnable {
	InvertedIndex invIdx = null;
	int startIndex = 0;
	int endIndex = 0;
	Set<Integer> twoWordDocs = null;
	int threadNum = 0;
	AtomicLong counter = null;

	public TwoCombinationGenerator(InvertedIndex invIdx, int startIndex,
			int endIndex, Set<Integer> twoWordDocs, int threadNum,
			AtomicLong totalCTR) {
		this.invIdx = invIdx;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.twoWordDocs = twoWordDocs;
		this.threadNum = threadNum;
		this.counter = totalCTR;
	}

	@Override
	public void run() {
		int f = 0;
		int total = 0;
		long prev = System.currentTimeMillis();
		for (int i = startIndex; i < endIndex; i++) {
			for (int j = i + 1; j < invIdx.numKeywords(); j++) {
				total++;
				int[] row1 = invIdx.getRow(i);
				int[] row2 = invIdx.getRow(j);
				int numMatchingDocs = 0;
				int candidateDoc = -1;
				for (int z = 0; z < row1.length; z++) {
					if (row1[z] > 0 && row2[z] > 0) {
						numMatchingDocs++;
						candidateDoc = z; // this might be a uniquely ID'd
											// document
					}

				}
				if (numMatchingDocs == 1) {
					twoWordDocs.add(candidateDoc);
				}
			}
			f++;
			if (f % 250 == 0) {
				System.out.println("Thread " + threadNum
						+ " has processed an additional 250 keywords in "
						+ (System.currentTimeMillis() - prev) + " millis.");
				prev = System.currentTimeMillis();
			}
		}
		System.out.println("Thread " + threadNum + "  processed " + total
				+ " combinations total.");
		counter.addAndGet(total);
	}

}
