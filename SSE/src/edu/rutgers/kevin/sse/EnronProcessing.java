package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Random;

 // visible to package members

/**
 * Read a set of documents from the enron email directories, and pass them on to
 * processing functions.
 */
public class EnronProcessing extends CorpusProcessing {

	public EnronProcessing(String rootPath, int nKeywords, SplitType splitType,
			float serverRatio, int randSeed) {
		TARGET_FOLDER = "_sent_mail";
		CORPUS_NAME = "enron";
		File docRoot = new File(rootPath);
		_randSeed = randSeed;
		_splitType = splitType;

		// Generate email file list and store top keyword list(s).
		if (splitType == SplitType.NONE) {
			noSplitProcessKeywords(nKeywords, docRoot);
		} else { // some kind of split, so two separate document sets.
			someSplitProcessKeywords(nKeywords, splitType, serverRatio,
					randSeed, docRoot);
		}
		// combined docIDMap is ok, because they're just for retrieval
		String docIdFileName = CORPUS_NAME+"_"+baseDocIDFilename;
		saveDocIDMap(docIdFileName);
	}

	@Override
	protected LinkedList<File> getAllFiles(File root) {
		LinkedList<File> files = new LinkedList<>();
		System.out.println("Reading email files from\n  " + root.getPath());
		int numDocs = 0;
		for (File userDir : root.listFiles()) {
			for (File curDir : userDir.listFiles()) {
				if (curDir.getAbsolutePath().contains(TARGET_FOLDER)) {
					for (File email : curDir.listFiles()) {
						if (email.isFile()) {
							_docIDMap.put(numDocs, email.getPath());
							files.add(email);
							numDocs++;
						} else { // debug
							System.out.println("** not a file: "
									+ email.getPath());
						}
					}
				}
			}
		}
		return files;
	}
	 /**
     * Split files randomly into two lists. serverRatio is the percentage of
     * documents the server gets. If subset is true, the client gets only the
     * rest, otherwise the client gets all.
     */
    protected void getFilesSplit(File root, float serverRatio, Boolean subset) {
        Random rng = new Random(_randSeed);
        _filesServer = new LinkedList<File>();
        _filesClient = new LinkedList<File>();
        System.out.println("Reading email files from\n  " + root.getPath());
        int numDocs = 0;
        for (File userDir : root.listFiles()) {
            for (File curDir : userDir.listFiles()) {
                if (curDir.getAbsolutePath().contains(TARGET_FOLDER)) {
                    for (File email : curDir.listFiles()) {
                        if (email.isFile()) {
                            _docIDMap.put(numDocs, email.getPath());
                            if (rng.nextFloat() < serverRatio) {
                                _filesServer.add(email);
                                if (subset) // then client searches all files.
                                {
                                    _filesClient.add(email);
                                }
                            } else {
                                _filesClient.add(email);
                            }
                            numDocs++;
                        }
                    }
                }
            }
        }
    }

	/**
	 * @param nKeywords
	 * @param splitType
	 * @param serverRatio
	 * @param randSeed
	 * @param docRoot
	 */
	private void someSplitProcessKeywords(int nKeywords,
			SplitType splitType, float serverRatio, int randSeed, File docRoot) {
		getFilesSplit(docRoot, serverRatio, (splitType == SplitType.SUBSET));
		System.out.println("File split: Server: "
				+ Integer.toString(_filesServer.size()) + " Client: "
				+ Integer.toString(_filesClient.size()));

		// get top keyword counts for training (server) set
		String kwFileServer = "kw_enron" + Integer.toString(nKeywords) + "_s"
				+ Integer.toString(randSeed) + "_"
				+ String.format("%.2f", serverRatio) + "frac_srv.txt";
		_kwCountServer = new KeywordCount(kwFileServer);
		if (!_kwCountServer.keywordFileRead()) {
			System.out.println("Generating server keyword list of size "
					+ Integer.toString(nKeywords));
			storeKeywordList(_kwCountServer, _filesServer, NSTOPWORDS,
					NSTOPWORDS + nKeywords);
		}

		// get top keyword counts for real (client) set
		String kwFileClient = "kw_enron"
				+ Integer.toString(nKeywords)
				+ "_s"
				+ Integer.toString(randSeed)
				+ "_"
				+ String.format("%.2f",
						(splitType == SplitType.DISJOINT) ? (1.0 - serverRatio)
								: 1.0) + "frac_cli.txt";
		_kwCountClient = new KeywordCount(kwFileClient);
		if (!_kwCountClient.keywordFileRead()) {
			System.out.println("Generating client keyword list of size "
					+ Integer.toString(nKeywords));
			storeKeywordList(_kwCountClient, _filesClient, NSTOPWORDS,
					NSTOPWORDS + nKeywords);
		}
		_filesClientIter = _filesClient.iterator();
	}

	/**
	 * @param nKeywords
	 * @param docRoot
	 */
	private void noSplitProcessKeywords(int nKeywords, File docRoot) {
		// get files
		_filesAll = getAllFiles(docRoot);
		System.out.println("Found " + Integer.toString(_filesAll.size())
				+ " email files");
		_filesAllIter = _filesAll.iterator();

		// keyword count - should have a parameter so don't always construct it?
		String keywordFile = "kw_enron_" + Integer.toString(nKeywords)
		/* + "_s" + Integer.toString(randSeed) */+ "_all.txt";
		// KeywordCount constructor will try to read the file.
		_kwCountAll = new KeywordCount(keywordFile);
		if (!_kwCountAll.keywordFileRead()) {
			storeKeywordList(_kwCountAll, _filesAll, NSTOPWORDS, NSTOPWORDS
					+ nKeywords);
		}
	}

	/**
	 * Command-line interface to computing and storing Enron statistics.
	 */
	public static void main(String[] args) {

		SplitType splitType = SplitType.NONE;
		String docRoot = "";
		int randSeed = 0;
		int numKeywords = 0;

		// read in parameters from properties file.
		Properties props = new Properties();
		String propsfile = "enron.properties";
		if (args.length > 0) {
			propsfile = args[0];
		}
		try {
			FileInputStream in_prop = new FileInputStream(propsfile);
			props.load(in_prop);
			in_prop.close();

			splitType = SplitType.valueOf(props.getProperty("split_type")
					.trim().toUpperCase());
			System.out.println("Read property splittype = "
					+ splitType.toString() + ".");
			docRoot = props.getProperty("doc_root").trim();
			// remove quotes
			if (docRoot.indexOf("\"") == 0
					&& docRoot.lastIndexOf("\"") == docRoot.length() - 1) {
				docRoot = docRoot.substring(1, docRoot.length() - 2);
			}
			System.out.println("Read property doc_root = \n  " + docRoot);
			randSeed = Integer.parseInt(props.getProperty("rand_seed").trim());
			System.out.println("Read property rand_seed = \n  "
					+ Integer.toString(randSeed));
			numKeywords = Integer.parseInt(props.getProperty("num_keywords")
					.trim());
			System.out.println("Read property num_keywords = \n  "
					+ Integer.toString(numKeywords));
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: could not read properties file");
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		// Go!
		EnronProcessing enron = new EnronProcessing(docRoot, numKeywords,
				splitType, (float) 0.5, randSeed);
		// TODO: separate functions for the cooccurrence and tfidf processing.
		enron.genTfidfStats();
	}

} // class EnronProcessing

