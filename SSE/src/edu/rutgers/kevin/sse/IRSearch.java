package edu.rutgers.kevin.sse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.zip.GZIPInputStream;
import porter.Stemmer;

/** Test class for single keyword query of a TfIdf object. */
public class IRSearch {

    public final static int NUM_QUERY_RESULTS = 10;
    
    public static void main(String[] args) {

        if (args.length != 2 && args.length != 3) {
            System.err.println("Usage: IRSearch <tfidf file.gz> [links file] <query>");
            System.exit(1);
        }
        
        TfIdf tfidf = null;
        System.out.println("Reading tfidf object from "+args[0]+" ...");
        try {
            FileInputStream fileIn = new FileInputStream(args[0]);
            GZIPInputStream gzIn = new GZIPInputStream(fileIn);
            ObjectInputStream in = new ObjectInputStream(gzIn);
            tfidf = (TfIdf) in.readObject();
            in.close();
            gzIn.close();
            fileIn.close();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }
        
        // load doc index links
        HashMap<Integer,String> docIDMap = null;
        if (args.length == 3) {
            System.out.println("Reading docID map object from "+args[1]+" ...");
            try {
                FileInputStream fileIn = new FileInputStream(args[1]);
                GZIPInputStream gzIn = new GZIPInputStream(fileIn);
                ObjectInputStream in = new ObjectInputStream(gzIn);
                docIDMap = (HashMap<Integer,String>) in.readObject();
                in.close();
                gzIn.close();
                fileIn.close();
            } catch (IOException e) {
                System.err.println("Could not read docID file " + args[1]);
                return;
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
                return;
            }
        }

        String qstring;
        if (args.length == 2) qstring = args[1]; else qstring = args[2];
        
        // Stem query keyword.
        Stemmer stemmer = new Stemmer();
        stemmer.add(qstring.toLowerCase().toCharArray(), qstring.length());
        stemmer.stem();
        String query=stemmer.toString();
        System.out.println("Stemmed query: " + query);

        Map<Integer, Double> result = tfidf.query(query, NUM_QUERY_RESULTS);
        if (result.isEmpty()) {
            System.out.println("Empty result set.");
        }
        else {
            System.out.println("**** Top 10 scored documents ****");
            for (int ix: result.keySet()) {
                System.out.println(Integer.toString(ix) + ": "
                                   + Double.toString(result.get(ix)) + "\n" + 
                                   ((docIDMap == null) ? "" : docIDMap.get(ix)));
            }
            System.out.println("**** Sorted scores *****");
            List<Double> scores = new ArrayList<Double>(result.values());
            Collections.sort(scores);
            System.out.print("[ ");
            for (double score: scores) {
                System.out.print(Double.toString(score) + ", ");
            }
            System.out.print("]\n");
        }
        
    } // main

} // class IRSearch
