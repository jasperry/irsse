package edu.rutgers.kevin.sse;

import java.util.HashMap;
import java.util.Map;
import java.util.Arrays; // for sort
import java.util.HashSet;
import java.util.Iterator;

// enums for parameters
enum TermOccurrence {NATURAL, LOGARITHM, AUGMENTED};
enum DocumentFrequency {NONE, T}; // not changed for docs.
enum Normalization {NONE, COSINE};

/** Class for computing and querying tf-idf term-doc matrix, by feeding it
    Document instances and a KeywordCount. */
class TfIdf implements java.io.Serializable {

    double[][] matrix = null;
    transient KeywordCount keywordCount; // only used during processing.
    Map<String,Integer> keywordMap = new HashMap<String,Integer>();
    Map<Integer,String> rowMap = new HashMap<Integer,String>();
    int maxKeyword = 0;
    int docCount = 0;
    TermOccurrence termOccurrence = TermOccurrence.NATURAL;
    Normalization normalization = Normalization.NONE;
    // probably need some doc-keyword counts

    // hmm, how to allocate the number of docs? make keywordCount save it.
    public TfIdf(KeywordCount kwCount) {
        keywordCount = kwCount;
        System.out.println("Initializing TfIdf matrix for " +
                           Integer.toString(keywordCount.size()) + " keywords, " +
                           Integer.toString(keywordCount.numDocs()) + " docs");
        matrix = new double[keywordCount.size()][keywordCount.numDocs()];//[35000];
    }

    /** Compute a new document's tf-idf, updating the counts */
    public void addDocument(Document doc) {

        for(Map.Entry<String,Integer> entry : doc.getKeywordCounts().entrySet()) {
            String word = entry.getKey();
            // If it's not in the counted set, skip.
            try {
                int wordCount = keywordCount.getCount(word);
            } catch (NullPointerException e) {
                // System.err.println("Keyword count for " + word + " not found");
                continue;
            }

            // intial version: ntn
            double tf, idf;
            if (termOccurrence == TermOccurrence.NATURAL) {
                tf = (double) entry.getValue();
            }
            else if (termOccurrence == TermOccurrence.LOGARITHM) {
                tf = 1.0 + Math.log((double) entry.getValue());
            }
            else {
                System.err.println("Warning: augmented tf not supported");
                tf = (double) entry.getValue();
            }
         
            idf = Math.log(((double) keywordCount.numDocs()) /
                           ((double)(keywordCount.getCount(word))));
            int keywordIndex;
            // create mapping of keywords to matrix rows as they appear.
            if (keywordMap.containsKey(word)) {
                keywordIndex = keywordMap.get(word);
            } else {
                // check if maxKeyword exceeds matrix rowsize?
                keywordIndex = maxKeyword;
                keywordMap.put(word, keywordIndex);
                rowMap.put(keywordIndex,word);
                maxKeyword++;
            }
            matrix[keywordIndex][docCount] = tf * idf;
        }
        docCount++;
    }

    /** Or I could initialize the keywordCount at construction.
        does anybody call this? */
    public double[][] getMatrix() {

        // sanity check. 
        /*if (docCount != maxKeyword) {// keywordCount.numDocs()) { //transient!
            System.out.println("Warning: mismatch in # of docs processed: " +
                               Integer.toString(docCount) + ", " +
                               Integer.toString(maxKeyword)); //keywordCount.numDocs())); 
                               } */
        return matrix;
    }

    /** Get the term string corresponding to a row in the matrix. */
    public String getRowKeyword(int row) {
        return rowMap.get(row);
    }

    private double[] getQueryRow(String stemmedKeyword) {

        if (matrix == null) return null;
        if (! keywordMap.containsKey(stemmedKeyword)) return null;
        int index = keywordMap.get(stemmedKeyword);
        return matrix[index];

    }

    /** Answer a keyword query by stemming and getting the docs with
     * top scores. */
    public Map<Integer,Double> query(String keyword, int nresults) {
        // derive DocumentImpl to get keyword counts?
        double[] queryRow = getQueryRow(keyword);
        System.out.println("\nSearching for stemmed word " + keyword);
        return topN(queryRow, nresults);

    }

    // static methods - don't have to be in this class, actually
    
    /** Find the key of the minimum value in the map. */
    private static int mapMin(HashMap<Integer, Double> m) {
        double curMin = Double.POSITIVE_INFINITY;
        int curMinIx = -1;
        for (int key: m.keySet()) {
            if (m.get(key) < curMin) {
                curMinIx = key;
                curMin = m.get(key);
            }
        }
        return curMinIx;
    }

    /** Return the top n values of an array (matrix row). Not specific to tfidf. */
    public static Map<Integer, Double> topN(double[] a, int n) {
        
        HashMap<Integer, Double> topScores = new HashMap<Integer,Double>();

        // add the first n elements to the map and keep the minimum.
        double curMin = a[0];
        int curMinIx = 0;
        for (int i=0; i < n; i++) {
            topScores.put(i, a[i]);
            if (a[i] < curMin) {
                curMin = a[i]; curMinIx = i;
            }
        }
        for (int i=n; i < a.length; i++) {
            if (a[i] > curMin) {
                    // add the new, then check the new minimum.
                    topScores.remove(curMinIx);
                    topScores.put(i, a[i]);
                    curMinIx = mapMin(topScores);
                    curMin = topScores.get(curMinIx);
                }
        }
        return topScores;
    }
    
    /** Return the top n entries from a row of the matrix
     * (corresponding to a keyword) */
    /* private int[] topNIndex(double[] a, int n) {
        HashSet<Integer> topSet = new HashSet<Integer>();

        // naive algorithm: get the top score n times
        for (int i=0; i < n; i++) {
            double curMax = Double.NEGATIVE_INFINITY;
            int maxIndex = -1; // must init; make it blow up if not found.
            for (int j = 0; j < a.length; j++) {
                if (a[j] > curMax && (! topSet.contains(j))) {
                    curMax = a[j];
                    maxIndex = j;
                }
            }
            topSet.add(maxIndex);
            // DEBUG
            System.out.println("top scores: " + Integer.toString(maxIndex) +
                               ": " + Double.toString(curMax));
        }
        int[] topNIndexes = new int[n];
        Iterator<Integer> iter = topSet.iterator();
        int i=0;
        while (iter.hasNext()) {
            topNIndexes[i] = iter.next();
            i++;
        }
        return topNIndexes;
        } */

}
