package edu.rutgers.kevin.sse;

import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;

import edu.jhu.nlp.wikipedia.PageCallbackHandler;
import edu.jhu.nlp.wikipedia.WikiPage;

/**
 * @brief PageCallbackHandler for indexing documents in Lucene.
 * @author paulgrubbs
 * 
 */
public class ArticleLuceneIndexerPageCallbackHandler implements
		PageCallbackHandler {
	IndexWriter idx;
	AtomicLong ctr;
	long prevMillis;
	private int maxProcessed;
	int printAfterThisMany = 1000;
	private static final Logger log = Logger
			.getLogger(ArticleLuceneIndexerPageCallbackHandler.class);

	public ArticleLuceneIndexerPageCallbackHandler(IndexWriter _idx, AtomicLong ctr, int maxProcessed) {
		this.idx = _idx;
		this.ctr = ctr;
		prevMillis = System.currentTimeMillis();
		this.maxProcessed = maxProcessed;
	}

	@Override
	public void process(WikiPage page) {
		if (!(page.isStub() || page.isDisambiguationPage() || page.isRedirect() || page
				.isSpecialPage())) {
			long curr = ctr.incrementAndGet();
			if (curr % printAfterThisMany == 0) {
				System.out.println("Parsed " + curr + " docs in "
						+ (System.currentTimeMillis() - prevMillis)
						+ " milliseconds");
				prevMillis = System.currentTimeMillis();
			}
			if (curr < maxProcessed) {
				String text = page.getWikiText();
				String title = page.getTitle().replaceAll(",", "")
						.replaceAll("\\.", "").replaceAll("/", "_").trim()
						.replace(" ", "_").toLowerCase();
				FileWriter f = null;
				String s = null;
				try {
					if (this.idx == null)
						throw new RuntimeException("IndexWriter is null.");
					org.apache.lucene.document.Document doc = new org.apache.lucene.document.Document();
					doc.add(new StringField("title", title, Field.Store.YES));
					doc.add(new TextField("text", text, Field.Store.NO));
					idx.addDocument(doc);
				} catch (Exception e) {
					log.error("Can't create file.", e);
					try {
						if (f != null) {
							f.close();
						}
					} catch (IOException e1) {
						log.error("Can't close file.", e1);
					}
				} finally {
					try {
						if (f != null) {

							f.flush();
							f.close();
						}
					} catch (Exception e1) {
						System.out.println("Can't flush or close file called "
								+ s);
						log.error(
								"Can't flush or close file called "
										+ page.getTitle(), e1);
					}

				}
			}else{
				//TODO: this is a hack, a better way would be to modify the parse method to take a maxDocumentsParsed argument
				throw new RuntimeException("Done processing.");
			}
		}
	}

}
