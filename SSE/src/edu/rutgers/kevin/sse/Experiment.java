package edu.rutgers.kevin.sse;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Random;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.Properties;

import ut.UTOptimizer;

public class Experiment {
    public static void main(String[] args) throws FileNotFoundException,
                                                  IOException {

        // experiment parameters, to be read from the properties file.
        int annealSeed;
        boolean apacheEmails;
        String apacheEmailsRoot;
        String docRoot;
        int numKeywords;
        int numQueries; // must be a big enough fraction of numKeywords
        int knownFraction; // reciprocal
        SplitType splitType;
        float serverRatio;
        // Sample queries uniformly from keywords (o.w. just take the top)
        boolean uniformQueries;
        // boolean trashQueryProbs = false;  // unprincipled.
        // double truncValue = 0.005;
        boolean writeMatrices;
        boolean topCountsAnneal;
        int topCountsPercent;
        boolean doAnnealing;
        double startTemp;
        double finalTemp;
        double coolingRate;
        double numReconstruct;

        // Open and read properties file
        Properties props = new Properties();
        String propsfile = "ikkexper.properties";
        //try {
        FileInputStream in_prop = new FileInputStream(propsfile);
        props.load(in_prop);
        in_prop.close();

        System.out.println("***** Experiment Parameters *****");
        splitType = 
            SplitType.valueOf(props.getProperty("split_type").trim()
                              .toUpperCase());
        serverRatio = Float.parseFloat(props.getProperty("server_ratio").trim());
        apacheEmails = Boolean.parseBoolean(props.getProperty("apache_emails"));
        apacheEmailsRoot = props.getProperty("apache_emails_root").trim();
        if (apacheEmailsRoot.indexOf("\"") == 0 && apacheEmailsRoot.lastIndexOf("\"") ==
            apacheEmailsRoot.length() - 1)
            apacheEmailsRoot = apacheEmailsRoot.substring(1, apacheEmailsRoot.length() - 2);
        docRoot = props.getProperty("doc_root").trim();
        // remove quotes
        if (docRoot.indexOf("\"") == 0 && docRoot.lastIndexOf("\"") ==
            docRoot.length() - 1)
            docRoot = docRoot.substring(1, docRoot.length() - 2);
        annealSeed = Integer.parseInt(props.getProperty("anneal_seed").trim());
        numKeywords =
            Integer.parseInt(props.getProperty("num_keywords").trim());
        numQueries =
            Integer.parseInt(props.getProperty("num_queries").trim());
        knownFraction =
            Integer.parseInt(props.getProperty("known_fraction").trim());
        uniformQueries =
            Boolean.parseBoolean(props.getProperty("uniform_queries").trim());
        topCountsAnneal = 
            Boolean.parseBoolean(props.getProperty("top_counts_anneal").trim());
        topCountsPercent = 
            Integer.parseInt(props.getProperty("top_counts_percent").trim());
        doAnnealing = Boolean.parseBoolean(props.getProperty("do_annealing").trim());
        writeMatrices = Boolean.parseBoolean(props.getProperty("write_matrices").trim());
        startTemp = Double.parseDouble(props.getProperty("start_temp").trim());
        finalTemp = Double.parseDouble(props.getProperty("final_temp").trim());
        coolingRate = Double.parseDouble(props.getProperty("cooling_rate").trim());
        numReconstruct = Integer.parseInt(props.getProperty("num_reconstruct").trim());
        System.out.println(" split_type = " + splitType.toString());
        System.out.println(" server_ratio = " + Float.toString(serverRatio));
        System.out.println(" doc_root =  \n" + docRoot);
        System.out.println(" anneal_seed =  " + Integer.toString(annealSeed));
        System.out.println(" num_keywords =  " + Integer.toString(numKeywords));
        System.out.println(" num_queries =  " + Integer.toString(numQueries));
        System.out.println(" known_fraction =  " + Integer.toString(knownFraction));
        System.out.println(" uniform_queries =  " + Boolean.toString(uniformQueries));
        System.out.println(" annealing parameters: " + Double.toString(startTemp) + ", " +
                           Double.toString(finalTemp) + ", " +
                           Double.toString(coolingRate));
        //} 
        /* catch (FileNotFoundException e) {
            System.err.println("ERROR: could not read properties file");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } */

        // 0. The object that processes the data to get the matrices.
        CorpusProcessing dataset;
        if (apacheEmails) {
            ApacheEmailProcessing apacheDataset = new ApacheEmailProcessing();
            //  This was the unzipping part. 
            // apacheDataset.preprocessApacheCorpus(new File(docRoot), "_proc");
            if (splitType == SplitType.NONE) {
                apacheDataset.noSplitProcessKeywords(numKeywords, 
                                                     new File(apacheEmailsRoot));
            }
            else {
                apacheDataset.someSplitProcessKeywords(numKeywords, splitType, serverRatio, 
                                                       annealSeed, new File(apacheEmailsRoot));
            }
            dataset = apacheDataset;
        }
        else {
            dataset = new EnronProcessing(docRoot, numKeywords,
                                          splitType, serverRatio,
                                          annealSeed);
        }
        // 1. Obtain the true co-occurrence probability matrix, and the keyword
        //    count object, from the enron data processor.
        ProbabilityMatrix clientCoocMatrix; 
        KeywordCount kwCountAll;
        if (splitType == SplitType.NONE) {
            System.out.println("Retrieving full coocurrence matrix for queries...");
            clientCoocMatrix = dataset.getMatrix(); 
            kwCountAll = dataset.getKeywordCount();
            // Client queries fulfilled from a separate dataset.
        } else { // client's subset (maybe all)
            System.out.println("Retrieving client cooccurrence matrix for queries...");
            clientCoocMatrix = dataset.getTestMatrix();
            kwCountAll = dataset.getClientKeywordCount();
        }
        numKeywords = clientCoocMatrix.size(); // replace previous value.
        
        // 2. Select queries without replacement from the top keyword list,
        //    saving the row mapping.        
        HashMap<Integer,Integer> queryRowMap = new HashMap<>();
        // TODO: separate seed for queries / anneal.
        Random rng = new Random(2222); // fixed seed for repeatability
        for (int i = 0; i < numQueries; i++) {
            if (uniformQueries) {
                int targ;
                do {
                    targ = rng.nextInt(numKeywords);
                } while (queryRowMap.containsValue(targ)); 
                queryRowMap.put(i, targ);
            } else {  // Just use the first words. DO NOT USE.
                queryRowMap.put(i,i); 
            }
        }
        // 3. Fill in the coocurrence submatrix for the queries.
        double[][] clientMatrixVals = clientCoocMatrix.getMatrix();
        double[][] queryMatrixVals = new double[numQueries][numQueries];
        for(int i=0; i < numQueries; i++) {
            int count = 0;
            for(int j=0; j < numQueries; j++) {
                // I believe the server observes the true value. If query i and
                //   j are both included, it will see all overlaps.
                // ...it sees the true raw count, but how about the prob?
                //  Yes, that too, because it 
                queryMatrixVals[i][j] = clientMatrixVals[queryRowMap.get(i)]
                                                        [queryRowMap.get(j)]; 
            }
        }
        
        // 4. Set the coocurrence matrix that the server knows. 
        //ProbabilityMatrix knownMatrix;
        double[][] knownMatrixVals;
        // original experiment: server knows true doc matrix.
        if (splitType == SplitType.NONE) {
            knownMatrixVals = clientCoocMatrix.getMatrix(); //clientMatrixVals should be fine, no?
        } else { // matrixFromTrainingSet
            System.out.println("Retrieving server (training set) matrix...");
            knownMatrixVals = dataset.getTrainMatrix().getMatrix();
            //knownMatrix.printMatrix();
        }
        // declare data structure of known queries
        ArrayList<String> knownQueries;

        // If we're doing the initial top-keywords-only anneal, get the new
        //    top-only query matrix and row map.
        if (topCountsAnneal){
            int nTopKeywords = (int) ((double) (topCountsPercent * numKeywords) / 100.0);
            System.out.println("Using top " + Integer.toString(nTopKeywords) + " keywords");
            double [][] clientTopMatrixVals = 
                    clientCoocMatrix.getTopKeywordMatrix(kwCountAll, nTopKeywords);
            // map of top query indexes into TOP co-occurrence matrix
            HashMap<Integer, Integer> topQueryRowMap = new HashMap<>();
            // map of top query indexes to original query matrix rows.
            HashMap<Integer, Integer> queryQueryMap = new HashMap<>();
            // map of keyword strings to rows in top co-occurrence matrix
            Map<String, Integer> topNKeywordMap = clientCoocMatrix.getTopNKeywordMap();
            int queryIndex = 0;
            for (int q : queryRowMap.keySet()) {
                // Fetch the keyword string from the full matrix's index.
                String kw = clientCoocMatrix.getKeywordString(queryRowMap.get(q));
                // If the query is one of the top keywords, add it.
                if (topNKeywordMap.containsKey(kw)) {
                    System.out.println("Adding top query: " + queryIndex + " " + kw + 
                            " , topMap row: " + topNKeywordMap.get(kw));
                    // Add the pointer to the row in the top keywords matrix.
                    topQueryRowMap.put(queryIndex, topNKeywordMap.get(kw)); 
                    // Add the pointer back to the original query.
                    queryQueryMap.put(queryIndex, q);
                    queryIndex++;
                }
            }
            int numTopQueries = topQueryRowMap.size();
            System.out.println("Queries among top keywords: " + 
                               Integer.toString(numTopQueries));
            // populate query-only co-occurrence matrix using the new map.
            double[][] topQueryMatVals = new double[numTopQueries][numTopQueries];
            for (int i = 0; i < numTopQueries; i++) {
                for (int j = 0; j < numTopQueries; j++) {
                    topQueryMatVals[i][j] = clientTopMatrixVals[topQueryRowMap.get(i)]
                                                               [topQueryRowMap.get(j)];
                }
            }
            
            // Previously I stopped here and set the data structures for the main anneal.
            /*
            numQueries = numTopQueries;
            queryRowIndex = topQueryRowMap;
            queryMatrixVals = topQueryMatVals;
            clientMatrixVals = clientTopMatrixVals;       
            */
            
            // Set known queries 
            ArrayList<String> knownTopQueries = new ArrayList<>();
            for(int i=0; i < topQueryMatVals.length / knownFraction; i++) {
                knownTopQueries.add(i + ":" + topQueryRowMap.get(i));
            }
            int numKnownQueries = knownTopQueries.size(); // before new results added.
            System.out.println("Added " + numKnownQueries + " known queries");
            // Do annealing
            // TODO: different if giving server different data.
            double[][] knownTopMatrixVals = clientTopMatrixVals;
            System.out.println("Annealing with top keywords, " + topQueryMatVals.length + 
                               " queries, " + knownTopMatrixVals.length + 
                               " known words...");
            UTOptimizer optimizer=new UTOptimizer(startTemp, finalTemp,
                                                  coolingRate); 
            optimizer.init(topQueryMatVals, knownTopMatrixVals, knownTopQueries, annealSeed);
            HashMap<Integer,Integer> result = optimizer.anneal();
            
            // use row maps to add the results to the list of known queries,
            int correct=0;
            knownQueries = new ArrayList<>(); // have to re-add the existing ones.
            System.out.println("Got " + result.size() + " results. Correct set: ");
            for (int r : result.keySet()) {
                int queryOriginalIndex = queryQueryMap.get(r);
                int resultOriginalRow = clientCoocMatrix.getTopNRowIndex(result.get(r));
                knownQueries.add(queryOriginalIndex + ":" + resultOriginalRow);

                //  also check if correct and print.
                //if(resultOriginalRow == queryRowMap.get(queryOriginalIndex)){
                if (result.get(r).equals(topQueryRowMap.get(r))) {
                    correct++;
                    System.out.print(//Integer.toString(key) + "=" +
                                     //Integer.toString(result.get(key)) + "(" +
                                     clientCoocMatrix.getKeywordString(resultOriginalRow) + ", ");
                }
            }
            // Print total number correct in first phase.
            System.out.println("\n********************************************");
            System.out.println
                ("Correct queries: " + Integer.toString(correct) +
                 " (rate: " + Double.toString
                 (Double.valueOf(correct - numKnownQueries) / 
                  Double.valueOf(numTopQueries - numKnownQueries)) + ")");
            System.out.println("********************************************");
            
            System.out.println("Set " + knownQueries.size() + 
                               " known queries after 1st phase");
        }  // if (topCountsAnneal)
        // TODO: if only top anneal, skip and print results. 
        
        // 5. if not doing the pre-anneal, set the known queries the original way.
        else { 
            knownQueries = new ArrayList<>();
            for(int i=0; i < queryMatrixVals.length / knownFraction; i++) {
                knownQueries.add(i + ":" + queryRowMap.get(i));
            }
        }
        
        // save matrices to disk if requested.
        if (writeMatrices) {
            String kwMatrixFilename = "mat_kw.csv";
            String queryMatrixFilename = "mat_query.csv";
            writeMatrix(knownMatrixVals, kwMatrixFilename);
            writeMatrix(queryMatrixVals, queryMatrixFilename);
            // write the true assignments.
            for (int i=0; i < queryMatrixVals.length; i++) {
                System.out.print(Integer.toString(queryRowMap.get(i)) + " ");
            }
            System.out.println();
        }

        // 6. Run annealer.
        if (doAnnealing) {
            System.out.println("Annealing...");
            UTOptimizer optimizer=new UTOptimizer(startTemp, finalTemp,
                                                  coolingRate); 
            optimizer.init(queryMatrixVals, knownMatrixVals, knownQueries, annealSeed);
            HashMap<Integer,Integer> result = optimizer.anneal();

            // 5. Parse and display results.
            // System.out.println(result);
            int correct=0;
            System.out.print("Correct set: ");
            for (Integer key:result.keySet()) {
                if(result.get(key).equals(queryRowMap.get(key))){
                    correct++;
                    System.out.print(Integer.toString(key) + "=" +
                                     Integer.toString(result.get(key)) + "(" +
                                     clientCoocMatrix.getKeywordString(result.get(key)) + "), ");
                }
            }
            int numKnownQueries = knownQueries.size(); // numQueries / knownFraction;
            System.out.println("\n********************************************");
            System.out.println
                ("Correct queries: " + Integer.toString(correct) +
                 " (rate: " + Double.toString
                 (Double.valueOf(correct - numKnownQueries) / 
                  Double.valueOf(numQueries - numKnownQueries)) + ")");
            System.out.println("Annealing values:  " + Double.toString(startTemp) +
                               ", "  + Double.toString(finalTemp) + ", " +
                               Double.toString(coolingRate) + ", seed=" +
                               Integer.toString(annealSeed));
            System.out.println("********************************************");
        

            // 6. get some document reconstructions.
            // How to pick documents that have lots of the query words in them?
            if (numReconstruct > 0) {
                List<Document> reconDocs = dataset.getSampleDocs();
                for (Document doc:reconDocs) {
                    // get the top keywords
                    // Map<String,Integer> kwCount = doc.getKeywordCounts();
                    System.out.println("Guessed Word        Actual Word");
                    System.out.println("------------       ------------");
                    Set<String> keywords = doc.getKeywords();
                    // Cycle through the queries.
                    for (Integer key:result.keySet()) {
                        String trueWord = clientCoocMatrix.getKeywordString(queryRowMap.get(key));
                        // If query is a word in the document, print it out.
                        if (keywords.contains(trueWord)) {
                            String guessedWord = clientCoocMatrix.getKeywordString(result.get(key));
                            System.out.println(Integer.toString(key) + ": " +
                                               guessedWord + "\t\t" + trueWord);
                        }
                    }
                    // print out the document itself to see.
                    System.out.println("\nActual Document\n---------------");
                    System.out.print(doc.getText());
                    System.out.println("");
                    //numSamples++; if (numSamples >= 5) break;
                }
            } // if (doc reconstruct)
        }  // if (doAnnealing)
    }

    /** Print a csv representation of a matrix. */
    public static void writeMatrix(double[][] matrix, String filename) {
        File file = new File(filename);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (double[] row : matrix) {
                for (int j = 0; j < row.length; j++) {
                    writer.write(Double.toString(row[j]) + ", ");
                }
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.err.println("Unable to write matrix file" + filename);
            
        }
    }
}
