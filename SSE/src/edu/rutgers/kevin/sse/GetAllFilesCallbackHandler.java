package edu.rutgers.kevin.sse;

import java.io.File;
import java.util.LinkedList;
import java.util.Map;

public class GetAllFilesCallbackHandler implements FileCallbackHandler {

	LinkedList<File> files;
	int numDocs = 0;
	Map<Integer, String> docIDMap;
	public GetAllFilesCallbackHandler(LinkedList<File> _accumulator, Map<Integer, String> __docIDMap){
		this.files = _accumulator;
		this.docIDMap = __docIDMap;
	}
	@Override
	public void process(File f) {
		docIDMap.put(numDocs, f.getPath());
		files.add(f);
		numDocs++;
	}

}
