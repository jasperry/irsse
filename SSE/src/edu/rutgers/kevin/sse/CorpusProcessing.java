package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import edu.rutgers.kevin.Pair;

public abstract class CorpusProcessing {
	protected final String baseDocIDFilename = "docID_all.ser.gz";
	protected final int NSTOPWORDS = 199;
	protected int _randSeed;
	protected SplitType _splitType;
	protected ProbabilityMatrix _matrix = null;
	protected ProbabilityMatrix _serverMatrix = null;
	protected ProbabilityMatrix _clientMatrix = null;
	protected KeywordCount _kwCountAll = null;
	protected KeywordCount _kwCountClient = null;
	protected KeywordCount _kwCountServer = null;
	protected String TARGET_FOLDER;
	protected String CORPUS_NAME;
	// map of document indexes to filenames.
	protected Map<Integer, String> _docIDMap = new HashMap<>();

	protected LinkedList<File> _filesServer = null; // corpus known by Server
	protected LinkedList<File> _filesClient = null; // queried by Client
	protected LinkedList<File> _filesAll = null;
	protected Iterator<File> _filesAllIter;
	protected Iterator<File> _filesClientIter;

	/** To save the keyword-row correspondence for the top-N matrix. */
	protected Map<String, Integer> _topNKeywordMap = new HashMap<>();

	// To fetch for reconstruction.
	protected LinkedList<Document> _sampleDocs = new LinkedList<>();
	protected abstract void getFilesSplit(File root, float serverRatio, Boolean subset);
	protected abstract LinkedList<File> getAllFiles(File root);
	/** Iterate over docs */
    public Boolean hasNextDoc() { return _filesAllIter.hasNext(); }
    public Document nextDoc() {
        return new OldEnronDocumentImpl(_filesAllIter.next());
    }
    public void resetDocIter() {
        _filesAllIter = _filesAll.iterator();
    }
    
    public Boolean hasNextClientDoc() { return _filesClientIter.hasNext(); }
    public Document nextClientDoc() {
        return new OldEnronDocumentImpl(_filesClientIter.next());
    }
    public void resetClientDocIter() {
        _filesClientIter = _filesClient.iterator();
    }
    
   

    public List<Document> getSampleDocs() {
        return _sampleDocs;
    }
    /**
     * Compute co-occurrence matrix for a list of documents
     */
    private ProbabilityMatrix
            computeCooccurProbs(KeywordCount keywordCount, List<File> docList) {
        ProbabilityMatrix matrix
                = new ProbabilityMatrixImpl(keywordCount.size());
        int numDocs = 0;
        for (File f : docList) {
            Document currentDoc;
            currentDoc = new OldEnronDocumentImpl(f, keywordCount);
            matrix.addDocument(currentDoc);
            currentDoc.close();
            if ((++numDocs) % 500 == 0) {
                System.out.print(Integer.toString(numDocs) + " ");
                _sampleDocs.add(currentDoc);  // breaks staticness.
            }
        }
        return matrix;
    }
            
    public List<Document> getRandomDocs(int n, Integer seed) {
        // need to way to refer to the docID string to see if it's known?
        if (seed == null) { seed = _randSeed; }
        List<Document> results = new LinkedList<>();
        Set<Integer> indexSet = new HashSet<>();
        Random rng = new Random(seed + n); // hack to make it different.
        while (indexSet.size() < n) {
            indexSet.add(rng.nextInt(_filesAll.size()));
        }
        Integer[] indexList = indexSet.toArray(new Integer[0]);
        for (int ix: indexList) {
            Document doc = new OldEnronDocumentImpl(_filesAll.get(ix));
            results.add(doc);
        }
        return results;
    }
    /**
     * Process entire dataset and write out keyword list
     */
    public void storeKeywordList(KeywordCount keywordCount, List<File> files,
            int startIndex, int endIndex) {
        generateKeywordCount(keywordCount, files);
        keywordCount.writeFile(startIndex, endIndex);
        // clear out everything but the top keywords.
        keywordCount.reset(startIndex, endIndex);
    }

    /** Return the full keyword count object */
    public KeywordCount getKeywordCount() { return _kwCountAll; }
    
    /** Return the client keyword count object */
    public KeywordCount getClientKeywordCount() { return _kwCountClient; }
    
    /** Return the server keyword count object */
    public KeywordCount getServerKeywordCount() { return _kwCountServer; }
    
    public int numDocs() { return _docIDMap.size(); }
    
    /**
     * Get the co-occurrence matrix computed from all documents. Redundant? Same
     * result as getTestMatrix unless disjoint
     */
    public ProbabilityMatrix getMatrix() {
        if (_matrix == null) {
            System.out.println("  Processing all docs for coocurrence probs...");
            // matrix=new ProbabilityMatrixImpl(_kwCountAll.size());
            _matrix = computeCooccurProbs(_kwCountAll, _filesAll);
        }
        return _matrix;
    }

    /** Return submatrix of only top n keywords */
    public double[][] getTopKeywordMatrix(int n) {
        if (_matrix == null) {
            System.out.println("  Processing all docs for coocurrence probs...");
            // matrix=new ProbabilityMatrixImpl(_kwCountAll.size());
            _matrix = computeCooccurProbs(_kwCountAll, _filesAll);
        }
        double[][] topMatrix = new double[n][n];
        final double[][] fullMatrix = _matrix.getMatrix();
        List<Pair<String, Integer>> topList = _kwCountAll.getTopKeywords();
        for (int i=0; i < n; i++) {
            String kw = topList.get(i).first();
            int rowindex = _matrix.getKeywordIndex(kw);
            // save the keyword-row correspondence OF THE NEW MATRIX.
            _topNKeywordMap.put(kw, i);
            //System.out.println("Adding top keyword row map: " + kw + ", " + Integer.toString(i));
            for (int j = 0; j < n; j++) {
                topMatrix[i][j] = fullMatrix[rowindex]
                                            [_matrix.getKeywordIndex(topList.get(j).first())];
            }
        }
        return topMatrix;
    }
    
    /**
     * Get the matrix made from the documents the server knows.
     */
    public ProbabilityMatrix getTrainMatrix() {
        if (_serverMatrix == null) {
            System.out.println("  Processing server docs for coocurrence probs...");
            _serverMatrix = computeCooccurProbs(_kwCountServer, _filesServer);
        }
        return _serverMatrix;
    }

    /**
     * Get the matrix made from the documents the client knows.
     */
    public ProbabilityMatrix getTestMatrix() {
        if (_clientMatrix == null) {
            System.out.println("  Processing client docs for coocurrence probs...");
            _clientMatrix = computeCooccurProbs(_kwCountClient, _filesClient);
        }
        return _clientMatrix;
    }

    /**
     * Write out the map from document numbers to file links.
     */
    protected void saveDocIDMap(String serFileName) {
        try {
            System.out.println("Serializing doc ID map to " + serFileName);
            FileOutputStream fileOut = new FileOutputStream(serFileName);
            GZIPOutputStream gzOut = new GZIPOutputStream(fileOut);
            ObjectOutputStream out = new ObjectOutputStream(gzOut);
            out.writeObject(_docIDMap);
            out.close();
            gzOut.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public String getDocLink(int docID) {
        return _docIDMap.get(docID);
    }
    
    public Map<String, Integer> getTopNKeywordMap() {
        return _topNKeywordMap;
    }
    
    /** Return the row of the (server?) matrix for a given keyword string */
    public int getTopNKeywordRow(String kw) {
        return _topNKeywordMap.get(kw);
    }

    
    /**
     * Add raw keyword counts to a KeywordCount object. Called by
     * storeKeywordList, only if file doesn't exist.
     */
    private void generateKeywordCount(KeywordCount keywordCount,
            List<File> docList) {
        int numDocs = 0;
        System.out.println("generating top keyword counts:\n");
        for (File f : docList) {
            Document currentDoc;
            currentDoc = new OldEnronDocumentImpl(f);
            keywordCount.addDocument(currentDoc);
            currentDoc.close(); // otherwise, hits open file limit.
            if ((++numDocs) % 1000 == 0) {
                System.out.print(Integer.toString(numDocs) + " ");
            }
        }
        System.out.println("Total keywords: " + keywordCount.size());
    }
    /** Serialize tf-idf matrix */
    private void saveTfIdf(TfIdf tfidf, String serFileName) {
        try {
            System.out.println("Serializing tfidf matrix to " + serFileName);
            FileOutputStream fileOut = new FileOutputStream(serFileName);
            GZIPOutputStream gzOut = new GZIPOutputStream(fileOut);
            ObjectOutputStream out = new ObjectOutputStream(gzOut);
            out.writeObject(tfidf);
            out.close();
            gzOut.close();
            fileOut.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    // for now this is handled by the getMatrix() functions. 
    public void genCooccurStats() {

    }
    /**
     * Compute and save inverted index from all files, with padding if requested
     */
    public InvertedIndex genInvertedIndex(int paddingMult) {
        InvertedIndex invInd = new InvertedIndex(_kwCountAll, paddingMult);
        int numDocs = 0;
        System.out.println("\nProcessing documents to compute inverted index:\n");
        for (File f : _filesAll) {
            Document currentDoc;
            currentDoc = new OldEnronDocumentImpl(f);
            invInd.addDocument(currentDoc);
            if ((++numDocs) % 1000 == 0) {
                System.out.print(Integer.toString(numDocs) + " ");
            }
            currentDoc.close();
        }
        System.out.println("Processed " + Integer.toString(numDocs)
                + " documents");
        if (paddingMult > 0) {
            invInd.addPadding();
        }
        return invInd;
    }
    
    /** Compute inverted index from client-side (indexed) data */
    public InvertedIndex genClientIndex() {
        InvertedIndex invInd = new InvertedIndex(_kwCountClient, 0);
        int numDocs = 0;
        for (File f : _filesClient) {
            Document currentDoc;
            currentDoc = new OldEnronDocumentImpl(f);
            invInd.addDocument(currentDoc);
            if ((++numDocs) % 1000 == 0) {
                System.out.print(Integer.toString(numDocs) + " ");
            }
            currentDoc.close();         
        }
        return invInd;
    }
    /**
	 * @brief Recursively process an entire directory tree.
	 * @param f
	 * @param accumulator
	 */
	protected void walkSubdirectories(File f, FileCallbackHandler thingToDo) {
		if (f.isDirectory()) {
                        System.out.println("step");
			String[] subNote = f.list();
			for (String filename : subNote) {
				walkSubdirectories(new File(f, filename), thingToDo);
			}
		} else {
			if (f.exists() && f.isFile()) {
				i++;
				if (i % printAfterThisManyDocuments == 0) {
					System.out.println("Processed an additional "
							+ printAfterThisManyDocuments + " documents for "
							+ i + " total documents.");

				}

				thingToDo.process(f);
			}
		}
	}
	private int i = 0;
	private int printAfterThisManyDocuments = 1000;
    // Compute and save tfidf matrices.
    public void genTfidfStats() {

        if (_splitType == SplitType.NONE) {
            TfIdf tfidf = computeTfIdf(_kwCountAll, _filesAll);
            String tfIdfFileName = "tfidf_all_"
                    + Integer.toString(_kwCountAll.size()) + ".ser.gz";
            saveTfIdf(tfidf, tfIdfFileName);
        } // if there's a split, _filesServer and _filesClient already
        //  contain the right sets.
        else {
            TfIdf tfidfTrain = computeTfIdf(_kwCountServer, _filesServer);
            String tfIdfFileName = "tfidf_train_"
                    + Integer.toString(_kwCountServer.size()) + ".ser.gz";
            saveTfIdf(tfidfTrain, tfIdfFileName);

            // stats for test set
            TfIdf tfidfTest = computeTfIdf(_kwCountClient, _filesClient);
            tfIdfFileName = "tfidf_test_"
                    + Integer.toString(_kwCountClient.size()) + ".ser.gz";
            saveTfIdf(tfidfTest, tfIdfFileName);
        }
    }

    // tfidf helper functions. could be abstracted out to handle ProbMatrix
    // and InvIndex too. 
    private TfIdf computeTfIdf(KeywordCount keywordCount, List<File> files) {
        TfIdf tfidf = new TfIdf(keywordCount);
        int numDocs = 0;
        System.out.println("\nProcessing documents to compute tfidf matrix:\n");
        for (File f : files) {
            Document currentDoc;
            currentDoc = new OldEnronDocumentImpl(f);
            tfidf.addDocument(currentDoc);
            if ((++numDocs) % 1000 == 0) {
                System.out.print(Integer.toString(numDocs) + " ");
            }
            currentDoc.close();
        }
        System.out.println("Processed " + Integer.toString(numDocs)
                + " documents");

        return tfidf;

    }

    
    
   

   
}
