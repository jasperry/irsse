package edu.rutgers.kevin.sse;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Collections;
import java.util.Arrays;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.zip.GZIPInputStream;
import porter.Stemmer;

public class IRAttack {

    public final static int NUM_QUERY_RESULTS = 10;

    public static Double scoreSSE(Double[] s1, Double[] s2) {
        Double sse = new Double(0.);
        for(int i=0; i < s1.length; i++) {
            double diff = s1[i] - s2[i];
            sse += diff * diff;
        }
        return sse;
    }

    /** See how many server values match for all the top client keywords */
    public static void attackAllQueries(TfIdf tfidf_server, TfIdf tfidf_client) {
        double[][] clientMatrix = tfidf_client.getMatrix();
        int matches = 0;
        for (int i=0; i < clientMatrix.length; i++) {
            Map<Integer,Double> rowTopN = tfidf_client.topN(clientMatrix[i],
                                                            NUM_QUERY_RESULTS);
            Double[] topNArray = rowTopN.values().toArray(new Double[rowTopN.size()]);
            int serverRow = closestQuery(topNArray, tfidf_server);
            if (tfidf_server.getRowKeyword(serverRow).equals(tfidf_client.getRowKeyword(i))) {
                System.out.println("Successfully matched keyword: "+
                                   tfidf_server.getRowKeyword(serverRow));
                matches++;
            }
        }
        System.out.println("!!! Correctly matched keywords: "+Integer.toString(matches));
    }

    /** Returns index of server keyword with closest-matching top-N scores */
    public static int closestQuery(Double[] queryScores, TfIdf tfidf) {
        double minSSE = Double.POSITIVE_INFINITY;
        int minSSEIx = -1;

        double[][] matrix = tfidf.getMatrix();
        for (int i=0; i < matrix.length; i++) {
            Map<Integer,Double> rowTopN = tfidf.topN(matrix[i], NUM_QUERY_RESULTS);
            Double[] topNArray = rowTopN.values().toArray(new Double[rowTopN.size()]);
            double sse = scoreSSE(queryScores, topNArray);
            if (sse < minSSE) {
                minSSE = sse;
                minSSEIx = i;
            }
        }
        // System.out.println("Closest distance found: " + Double.toString(minSSE));
        return minSSEIx;
    }

    /** Command-line interface to SSE attacks. */
    public static void main(String[] args) {
        System.out.println("Grr, attacking...");

        String qstring = "";
        String qstring2 = "";
        if (args.length >= 3) {
            qstring = args[2]; // to fail early
            if (args.length >= 4) qstring2 = args[3];
            else qstring2 = qstring;
        }
        
        // Read in the tfidf objects for client and server corpora.
        TfIdf tfidf_server, tfidf_client;
        System.out.println("Reading server corpus stats from "+args[0]);
        System.out.println("Reading client corpus stats from "+args[1]);
        try {
            FileInputStream fileIn = new FileInputStream(args[0]);
            GZIPInputStream gzIn = new GZIPInputStream(fileIn);
            ObjectInputStream in = new ObjectInputStream(gzIn);
            tfidf_server = (TfIdf) in.readObject();
            in.close();
            gzIn.close();
            fileIn.close();
            fileIn = new FileInputStream(args[1]);
            gzIn = new GZIPInputStream(fileIn);
            in = new ObjectInputStream(gzIn);
            tfidf_client = (TfIdf) in.readObject();
            in.close();
            gzIn.close();
            fileIn.close();

        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
            return;
        }

        // If queries exist, stem and run them
        if (! qstring.equals("")) {
            Stemmer stemmer = new Stemmer();
            stemmer.add(qstring.toLowerCase().toCharArray(), qstring.length());
            stemmer.stem();
            String query=stemmer.toString();
            System.out.println("Stemmed query (server): " + query);
            stemmer.add(qstring2.toLowerCase().toCharArray(), qstring2.length());
            stemmer.stem();
            String query2=stemmer.toString();
            System.out.println("Stemmed query (client): " + query2);

            Map<Integer, Double> result_server = tfidf_server.query(query, NUM_QUERY_RESULTS);
            if (result_server.isEmpty()) {
                System.out.println("Empty result set for server.");
            }
            Map<Integer, Double> result_client = tfidf_client.query(query2, NUM_QUERY_RESULTS);
            if (result_server.isEmpty()) {
                System.out.println("Empty result set for client.");
            }

            Double[] scores_server = result_server.values().toArray(new Double[0]);
            // new ArrayList<Double>(result1.values());
            Double[] scores_client = result_client.values().toArray(new Double[0]);
            //new ArrayList<Double>(result2.values());
            Arrays.sort(scores_server);
            Arrays.sort(scores_client);
            
            for (Double score: scores_server) {
                System.out.print(Double.toString(score) + ", ");
            }
            System.out.print("]\n");
            for (Double score: scores_client) {
                System.out.print(Double.toString(score) + ", ");
            }
            System.out.print("]\n");
            
            System.out.println("SSE of client/server scores: "
                               + Double.toString(scoreSSE(scores_server,
                                                          scores_client)));
            
            int serverRow = closestQuery(scores_client, tfidf_server);
            System.out.println("*** Closest server keyword to client query: "
                               + tfidf_server.getRowKeyword(serverRow));
        } // if (query)
        
        // TODO: based on experiment type, generate random queries
        attackAllQueries(tfidf_server, tfidf_client);        

    }

}

