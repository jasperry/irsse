package edu.rutgers.kevin.sse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.mail.RFC822Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Sets;

public class NewEnronDocumentImpl extends EmailDocumentImpl {

	private static final Logger log = Logger
			.getLogger(NewEnronDocumentImpl.class);

	@Override
	public String getText() {
		checkText();
		return text;
	}

	private void checkText() {
		if (text == null) {
			try {
				text = getBodyOfRFC822Email(f);
			} catch (Exception e) {
				log.error("Cannot get keywords.", e);
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public Set<String> getKeywords() {
		checkText();
		if (this.keywords != null)
			return this.keywords;
		List<String> kws;
		try {
			kws = tokenizeString(text);
		} catch (IOException e) {
			log.error("Cannot get keywords.", e);
			throw new RuntimeException(e);
		}
		this.keywords = Sets.newHashSet(kws);
		return this.keywords;
	}

	@Override
	public Map<String, Integer> getKeywordCounts() {

		if (keywordCounts != null) {
			return keywordCounts;
		}
		keywordCounts = new HashMap<String, Integer>();
		checkText();

		List<String> kws;
		try {
			kws = tokenizeString(text);
		} catch (IOException e) {
			log.error("Cannot get keywords.", e);
			throw new RuntimeException(e);
		}
		Multiset<String> bag = HashMultiset.create(kws);
		for (String s : bag.elementSet()) {
			keywordCounts.put(s, bag.count(s));
		}
		return keywordCounts;
	}

	public NewEnronDocumentImpl(File file) {
		_path = file.getPath();
		f = file;
		az = new EmailAnalyzer();
	}

	public NewEnronDocumentImpl(File file, KeywordCount filter) {
		_path = file.getPath();
		this.f = file;
		this.keywordWhitelist = filter;
		az = new EmailAnalyzer(filter.getAllCounts().keySet());
	}

	public static String getBodyOfRFC822Email(File f) throws IOException,
			SAXException, TikaException {
		InputStream is = null;
		try {
			Parser p = new RFC822Parser();
			is = new FileInputStream(f);
			Metadata md = new Metadata();
			BodyContentHandler hd = new BodyContentHandler(2 * 1024 * 1024);
			ParseContext pc = new ParseContext();
			p.parse(is, hd, md, pc);
			return hd.toString();
		} finally {
			if (is != null) {
				is.close();
			}
		}

	}

}
