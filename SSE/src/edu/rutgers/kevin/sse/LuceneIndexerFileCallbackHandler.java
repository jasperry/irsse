package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.lucene.index.IndexWriter;
/**
 * @brief Writes an individual file to a Lucene index. 
 * @author paulgrubbs
 *
 */
public class LuceneIndexerFileCallbackHandler implements FileCallbackHandler {
	private static final Logger log = Logger
			.getLogger(LuceneIndexerFileCallbackHandler.class);
	IndexWriter idx = null;

	public LuceneIndexerFileCallbackHandler(IndexWriter idx) {
		this.idx = idx;
	}

	@Override
	public void process(File f) {

		String title = f.getName();
		FileReader fr = null;
		try {
			fr = new FileReader(f);
			WikipediaProcessing.writeFileToLuceneIndex(title, fr, idx);
		} catch (IOException e) {
			log.error("Exception!", e);
			try {
				if (fr != null)
					fr.close();
			} catch (IOException e1) {
				log.error("Exception!", e1);
			}
		} finally {
			if (fr != null)
				try {
					fr.close();
				} catch (IOException e) {
					log.error("Exception!", e);
				}
		}
	}
}
