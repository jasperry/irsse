package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.log4j.Logger;

import edu.jhu.nlp.wikipedia.PageCallbackHandler;
import edu.jhu.nlp.wikipedia.WikiPage;

/**
 * @brief PageCallbackHandler for extracting documents from XML and writing them
 *        into their own file.
 * @note This creates multiple directories under the base directory given in the constructor.
 * @author paulgrubbs
 * 
 */
public class ArticleWriterPageCallbackHandler implements PageCallbackHandler {
	private static final int DIR_SIZE = 10000;
	String basedir;
	AtomicLong ctr;
	long prevMillis;
	int maxProcessed;
	int printAfterThisMany = 1000;
	private static final Logger log = Logger
			.getLogger(ArticleWriterPageCallbackHandler.class);

	public ArticleWriterPageCallbackHandler(String _basedir, AtomicLong ctr,
			int maxProcessed) {
		this.basedir = _basedir;
		this.ctr = ctr;
		this.maxProcessed = maxProcessed;
		prevMillis = System.currentTimeMillis();
	}

	@Override
	public void process(WikiPage page) {
		if (!(page.isStub() || page.isDisambiguationPage() || page.isRedirect() || page
				.isSpecialPage())) {
			long curr = ctr.incrementAndGet();

			if (curr % printAfterThisMany == 0) {
				log.info("Parsed " + curr + " docs in "
						+ (System.currentTimeMillis() - prevMillis)
						+ " milliseconds");
				prevMillis = System.currentTimeMillis();
			}

			String text = page.getWikiText();
			String title = page.getTitle().replaceAll(",", "")
					.replaceAll("\\.", "").replaceAll("/", "_").replaceAll("'", "").trim()
					.replace(" ", "_").toLowerCase();
			FileWriter f = null;
			String s = null;
			try {
				File currDir = new File(basedir + "/"+ (curr / DIR_SIZE));
				if(!currDir.exists())
					currDir.mkdir();
				s = currDir +"/"+ title;
				File toWrite = new File(s);
				if (!toWrite.exists()) {
					f = new FileWriter(toWrite);
					f.write(text);
				}
			} catch (Exception e) {
				log.error("Can't create file.", e);
				try {
					if (f != null) {
						f.close();
					}
				} catch (IOException e1) {
					log.error("Can't close file.", e1);
				}
			} finally {
				try {
					if (f != null) {

						f.flush();
						f.close();
					}
				} catch (Exception e1) {
					
					log.error(
							"Can't flush or close file called "
									+ page.getTitle(), e1);
				}
			}

		}
	}

}
