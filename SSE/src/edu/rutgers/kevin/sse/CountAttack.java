package edu.rutgers.kevin.sse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Properties;
import java.util.Random;
import java.util.HashMap;
import java.util.TreeMap;

enum QueryDistro { UNIFORM, ZIPF, ANTIZIPF }

/** To carry out the results-list-length attack */
class CountAttack {
    
    private InvertedIndex _invIdx;
    ArrayList<HashSet<Integer>> _countBuckets;
    int _randSeed;
    Random _rng;
    int _numKeywords;
    int _numQueries;
    int _paddingMult;
    int _serverDocKnowledgePercent;
    Integer[] _skipList;
    int[][] _queryMatrix;
    /** Map of queries to their rows in the term-doc matrix. */
    Integer[] _queryIndexes;
    HashMap<Integer, Integer> _knownQueries;

    public CountAttack(int randSeed, int numKeywords, int paddingMult,
                        int serverDocKnowledgePercent) {
            _randSeed = randSeed;
            _rng = new Random(_randSeed);
            _numKeywords = numKeywords;
            _paddingMult = paddingMult;
            _serverDocKnowledgePercent = serverDocKnowledgePercent;
    }
    
    public static void main(String[] args) throws FileNotFoundException,
                                                  IOException {
       
        // experiment parameters, to be read from the properties file.
        boolean apacheEmails;
        String apacheEmailsRoot;
        int randSeed;
        String docRoot;
        int numKeywords;
        int queryPercent;
        int paddingMult;
        int serverDocKnowledgePercent;
        int initialKnownQueries;
        QueryDistro queryDistro = QueryDistro.UNIFORM; // for future use
        boolean writeMatrices = false;
        
        // read properties
        Properties props = new Properties();
        String propsfile = "countAttack.properties";
        /*try (  */
        FileInputStream in_prop = new FileInputStream(propsfile);
        props.load(in_prop);
        in_prop.close();

        apacheEmails = Boolean.parseBoolean(props.getProperty("apache_emails"));
        apacheEmailsRoot = props.getProperty("apache_emails_root").trim();
        if (apacheEmailsRoot.indexOf("\"") == 0 && apacheEmailsRoot.lastIndexOf("\"") ==
            apacheEmailsRoot.length() - 1)
            apacheEmailsRoot = apacheEmailsRoot.substring(1, apacheEmailsRoot.length() - 2);
        docRoot = props.getProperty("doc_root").trim();
        // remove quotes
        if (docRoot.indexOf("\"") == 0 && docRoot.lastIndexOf("\"") ==
            docRoot.length() - 1)
            docRoot = docRoot.substring(1, docRoot.length() - 2);
        randSeed = Integer.parseInt(props.getProperty("rand_seed").trim());
        numKeywords =
            Integer.parseInt(props.getProperty("num_keywords").trim());
        queryPercent =
            Integer.parseInt(props.getProperty("query_percent").trim());
        serverDocKnowledgePercent = 
            Integer.parseInt(props.getProperty("server_knowledge_percent").trim());
        queryDistro = QueryDistro.valueOf(props.getProperty("query_distro").trim()
                                          .toUpperCase());
        writeMatrices = Boolean.parseBoolean(props.getProperty("write_matrices").trim());
        paddingMult = Integer.parseInt(props.getProperty("padding_mult"));
        initialKnownQueries = Integer.parseInt(props.getProperty("initial_known_queries")
                                                    .trim());

        // Go!
        CountAttack countAttack = new CountAttack(randSeed, numKeywords, paddingMult,
                                                  serverDocKnowledgePercent);
        if (apacheEmails) docRoot = apacheEmailsRoot;
        countAttack.loadDatasetGenIndex (docRoot, apacheEmails);
        countAttack.genRandomQueries(queryPercent, initialKnownQueries);
        if (serverDocKnowledgePercent == 100) {
            countAttack.fullKnowledgeQueryRecovery();
        }
        else {
            countAttack.partialKnowledgeQueryRecovery();
        }
        // show parameters at end, for ease of reference.
        System.out.println("***** Count Experiment Parameters *****");
        System.out.println(" doc_root =  \n" + docRoot);
        System.out.println(" rand_seed =  " + Integer.toString(randSeed));
        System.out.println(" num_keywords =  " + Integer.toString(numKeywords));
        System.out.println(" query_percent =  " + Integer.toString(queryPercent));
        System.out.println(" padding_mult =  " + Integer.toString(paddingMult));
        System.out.println(" initial_known_queries = " + 
                Integer.toString(initialKnownQueries));
        System.out.println(" server_knowledge_percent = " + 
                Integer.toString(serverDocKnowledgePercent));
      
    }
    
    
    /** Load corpus, generate inverted index and count buckets. */
    public void loadDatasetGenIndex(String docRoot, boolean apache) {
        
        CorpusProcessing dataset;
        // load and index the Enron dataset
        if (apache) {
            ApacheEmailProcessing apacheDataset = new ApacheEmailProcessing();
            // apacheDataset.preprocessApacheCorpus(new File(docRoot), "_proc");
            apacheDataset.noSplitProcessKeywords(_numKeywords, new File(docRoot));
            dataset = apacheDataset;
        }
        else {
            dataset = new EnronProcessing(docRoot, _numKeywords, SplitType.NONE,
                                          (float) 1.0, _randSeed);
        }
        _invIdx = dataset.genInvertedIndex(_paddingMult);

        // If server has partial knowledge of documents, make a skip list.
        if (_serverDocKnowledgePercent < 100) {
            int numUnknownDocs = dataset.numDocs() - 
                    (int) ((double) _serverDocKnowledgePercent / 100.0 * dataset.numDocs());
            System.out.println("Server partial knowledge " + _serverDocKnowledgePercent +
                               "%, " + numUnknownDocs + " unknown docs");
            // Make a set, sort it, to array.
            Set<Integer> skipList = new HashSet<>(numUnknownDocs);
            Random rng = new Random(_randSeed + 123);
            while (skipList.size() < numUnknownDocs) {
                skipList.add(rng.nextInt(dataset.numDocs()));
            }
            _skipList = skipList.toArray(new Integer[0]);
            Arrays.sort(_skipList);
        }
        
        // Populate counts buckets for server's known keywords.
        final int numCountBuckets = 15000; // should be greater than the maximum count 
        _countBuckets =  new ArrayList<>(numCountBuckets);
        for (int i = 0; i < numCountBuckets; i++) {
            _countBuckets.add(new HashSet<Integer>());
        }
        for (int i = 0; i < _invIdx.numKeywords(); i++) {
            int rowCount = (_serverDocKnowledgePercent == 100) ? 
                    (_invIdx.getRowCount(i)) : (_invIdx.getRowCountSkipList(i, _skipList));
            _countBuckets.get(rowCount).add(i);
        }
        
        int numUnique = 0;
        for (int i = 0; i < numCountBuckets; i++) {
            if (_countBuckets.get(i).size() == 1) numUnique++;
        }
        System.out.println("** Num unique counts among all keywords: " + 
                           Integer.toString(numUnique));
    }
    
    
    public void genRandomQueries(int queryPercent, int initialKnownQueries) {
        // Generate random queries (rows)
        _numQueries = (int) ((double) queryPercent / 100.0 * (double) _numKeywords);
        System.out.println("Generating " + Integer.toString(_numQueries) + " queries..."); 
        Set<Integer> queryset = new HashSet<>();
        // Select queries without replacement from the top keyword list.
        int query;
        do {
            query = _rng.nextInt(_numKeywords);
            queryset.add(query);
            //System.out.print(Integer.toString(query) + ", ");
        } while (queryset.size() < _numQueries);
        // Converted to array to make an ordered list of queries.
        _queryIndexes = queryset.toArray(new Integer[0]);
        // Print the list of query keyword numbers, for visual verification
        /* for (int i = 0; i < _queryIndexes.length; i++) {
            System.out.print(Integer.toString(_queryIndexes[i]) + ", ");
        }*/
        System.out.println();

        // copy the chosen rows into the query matrix. This is the structure that the
        //  server will attack.
        System.out.println("Populating query matrix of size " +
                           Integer.toString(_numQueries) + " x " +
                           Integer.toString(_invIdx.numDocs()) +
                           ". # nonzero entries:");        
        _queryMatrix = new int[_numQueries][_invIdx.numDocs()];
        for (int i = 0; i < _numQueries; i++) {
            _queryMatrix[i] = _invIdx.getRow(_queryIndexes[i]);
            // show the counts, just to check.
            System.out.print(Integer.toString(countNonZero(_queryMatrix[i])) + ", ");
        }
        System.out.println();
            
        // Also add initial known queries to knownQueries, if any
        _knownQueries = new HashMap<>();
        System.out.print("Initial known queries (" + initialKnownQueries + "): ");
        for (int i = 0; i < initialKnownQueries; i++) {
            // use same RNG.
            int q;
            do {
                q = _rng.nextInt(_numQueries);
            } while (_knownQueries.containsKey(q));
            _knownQueries.put(q, _queryIndexes[q]);
            System.out.print("(" + q + ", " + _queryIndexes[q] + ")" );
        }
        System.out.println();

    }
    
    
    /** Query recovery attack when server knows only partial index. */
    public void partialKnowledgeQueryRecovery() {
        
        // Wait, if i have some knownQueries, do I skip this altogether?
        if (! _knownQueries.isEmpty()) {
            recoverQueriesFromCoocs();
            return;
        }
        
        // sort query and keyword indices by decreasing counts. 
        //  For keywords, using countBuckets
        List<Integer> keywordRowsBySize = new ArrayList<>(_invIdx.numKeywords());
        for (int i = _countBuckets.size() - 1; i >= 0; i--) {
            for (int row : _countBuckets.get(i)) {
                keywordRowsBySize.add(row);
            }
        }
        // For queries, use a TreeMap
        TreeMap<Integer,Integer> queryRowSizeMap = new TreeMap<>();
        for (int i = 0; i < _queryMatrix.length; i++) {
            queryRowSizeMap.put(countNonZero(_queryMatrix[i]), i);
        }
        List<Integer> queryRowsBySize = new ArrayList<>(_queryMatrix.length);
        for (int count : queryRowSizeMap.descendingKeySet()) {
            System.out.println("(" + count + ", " + queryRowSizeMap.get(count) + ")");
            queryRowsBySize.add(queryRowSizeMap.get(count));
        }
        
      
        // Loop through queries
        for (int queryRow : queryRowsBySize) {
            //   for candidates (not in knownQueries)
            for (int keywordRow : keywordRowsBySize) {
                
            }
        }
        //     add to knownQuery and try co-occurrence counting
    }
    
    
    public void fullKnowledgeQueryRecovery() {
    
        // attack step 1: go through and make a list of queries with unique counts.
        int uniqueCountQueries = 0;
        final int maxMultiCount = 40; // big enough to get to fail in other ways...
        List<ArrayList<Integer>> multiCountQueries = new ArrayList<>(maxMultiCount+1);
        for (int i=0; i <= maxMultiCount; i++) {
            multiCountQueries.add(new ArrayList<Integer>());
        }
        for (int i=0; i < _queryMatrix.length; i++) {
            int count = countNonZero(_queryMatrix[i]);
            // NOTE: if padding is used, the counts are from the padded index.
            //  this is equivalent to the server rounding its counts up.
            int numCountMatches = _countBuckets.get(count).size();
            if (numCountMatches == 1) {
                _knownQueries.put(i, _queryIndexes[i]);
                uniqueCountQueries++;
            }
            else if (numCountMatches <= maxMultiCount) {
                multiCountQueries.get(numCountMatches).add(i);
            }
        }
        System.out.println("Found " + uniqueCountQueries + " unique-result-length queries");
        //int paddingStartCol = _invIdx.numDocs(); // enron.numDocs(); // the true (unpadded) number of docs
        
        // Step 2: if the number of unique counts is zero, try guess-and-check.
        if (_knownQueries.isEmpty()) {
            // Dump all multi-counts in one list (should preserve order.)
            for (int i = 2; i <= maxMultiCount; i++) {
                System.out.println("Number of queries with only " + i + " length matches: "
                                   + multiCountQueries.get(i).size());
                multiCountQueries.get(0).addAll(multiCountQueries.get(i));
            }
            // Run the co-occurrence matching code with each possible match.
            System.out.println(" No unique lengths, making guesses for known query...");
            for (int i: multiCountQueries.get(0)) {
                HashSet<Integer> candidates_i = 
                        _countBuckets.get(countNonZero(_queryMatrix[i]));
                for (int cand : candidates_i) {
                    _knownQueries.put(i, cand);
                    if (! recoverQueriesFromCoocs()) {
                        System.out.println("Guess failed...");
                        _knownQueries.clear();
                    }
                    else {
                        System.out.println("Success by guessing.");
                        return; //break;
                    }
                }
                System.out.println("All matches failed for first query...shouldn't happen");
                return;
            }
        } 
        recoverQueriesFromCoocs();
    }
    
    /** Second stage of count attack, can be called iteratively. 
      * @return false if inconsistency detected (wrong guess) */
    private boolean recoverQueriesFromCoocs() {
                                           
        // now the co-occurrence part of the attack.
        System.out.println("Starting cooc counts with " + _knownQueries.size() + 
                           " known queries");
        int paddingStartCol = _invIdx.numDocs();
        int nQueries = _queryMatrix.length;
        int numKnownPrev = 0;
        // continue until we know them all or stop improving. 
        int nruns = 0;
        while (_knownQueries.size() < nQueries &&
               _knownQueries.size() > numKnownPrev) {
            
            numKnownPrev = _knownQueries.size();
            for (int q = 0; q < nQueries; q++) {
                // skip over known
                if (_knownQueries.containsKey(q)) continue;
                System.err.print(" | try q# " + Integer.toString(q) + " "); 
                                // " (index row: " + Integer.toString(queries[q]) + ")");
                // query match candidates are indexes into the server matrix. 
                Set<Integer> kwCands;
                if (_serverDocKnowledgePercent == 100) {
                    kwCands = new HashSet<>(_countBuckets.get(countNonZero(_queryMatrix[q])));
                } 
                // partial knowledge: can't use exact count match, 
                //  must add all in the possible range.
                else { 
                    kwCands = new HashSet<>();
                    int queryNonZero = countNonZero(_queryMatrix[q]);
                    // server must have at least as many nonzeroes as query - skiplist size.
                    int countIdx = queryNonZero - _skipList.length;
                    if (countIdx < 0) countIdx = 0;
                    // server can't have more nonzeroes than query. 
                    for (; countIdx <= queryNonZero; countIdx++) {
                        kwCands.addAll(_countBuckets.get(countIdx));
                    }
                }
                System.err.print("from " + Integer.toString(kwCands.size()) +
                                 " cands, count " + 
                                 Integer.toString(countNonZero(_queryMatrix[q])));
                if (kwCands.isEmpty()) {
                    System.out.println("*Error: no matching candidates");
                    continue;
                }
                // count cooccurrences of qcand and each known query q2.
                // if it's not the same number as q and the known query, eliminate.
                int foundone = -1;
                int numTries = 0;
                for (int q2: _knownQueries.keySet()) {
                    List<Integer> removeList = new ArrayList<>();
                    //System.out.println("Checking cooccurrence match against term " +
                    //                   Integer.toString(q2));
                    for (int kwCand : kwCands) {
                        // get co-occurrence counts from query and server matrices and compare.
                        int coocQ = countCooccurrences(_queryMatrix[q],
                                                       _queryMatrix[q2]);
                        int coocCand;
                        boolean match;
                        // Method of server-side counting and matching differs by experiment type.
                        if (_serverDocKnowledgePercent < 100) {
                            coocCand = countCoocSkipList(_invIdx.getRow(kwCand),
                                                          _invIdx.getRow(_knownQueries.get(q2)),
                                                          _skipList);
                            match = coocCand <= coocQ  && coocQ <= coocCand + _skipList.length; 
                        }
                        else { 
                            coocCand = countCoocNoPadding(_invIdx.getRow(kwCand),
                                                          _invIdx.getRow(_knownQueries.get(q2)),
                                                          paddingStartCol);
                            match = matchCoocCount(coocQ, coocCand, 
                                           _invIdx.getRowCountNoPadding(kwCand),
                                           _invIdx.getRowCountNoPadding(_knownQueries.get(q2)),
                                           _paddingMult);
                        }
                        if (! match) removeList.add(kwCand);
                    }
                    // Remove all rejected candidates (couldn't do it in the loop)
                    for (int remove: removeList) {
                        kwCands.remove(remove);
                    }
                    numTries++;
                    // If this happens there was an inconsistency due to a wrong guess.
                    if (kwCands.isEmpty()) {
                        System.out.println("Error: all candidates eliminated");
                        return false;
                    }
                    if (kwCands.size() == 1) {
                        foundone = kwCands.toArray(new Integer[0])[0];
                        _knownQueries.put(q, foundone);
                        System.out.print("..Match! row " + Integer.toString(foundone) + 
                                         ", " + numTries + " tries");
                        // Need to verify match? Error probability is quite small, right?
                        break;
                    }
                }

                if (foundone == -1) {
                    System.out.println("..MATCH FAILED");
                }
            } // for (queries)
            nruns++;
        } // while (knownQueries.size() increasing.)  

        System.out.println("* Stopped after " + Integer.toString(nruns) + " runs");
        System.out.println(" Reconstructed " +
                           Integer.toString(_knownQueries.size()) +
                           " of " + Integer.toString(nQueries) + " queries.");
        System.out.println("Index size: " + _invIdx.indexSize() + " Padding size: " + 
                           _invIdx.paddingSize() + " blowup: " + 
                           ((double) (_invIdx.indexSize() + _invIdx.paddingSize()) / 
                            (double) (_invIdx.indexSize())));
        
        return true;
    }
    
    /** Determine whether a co-occurrence count is compatible with a count from a padded 
     *  index. */
    private static boolean matchCoocCount(int paddedCount, int candCount, 
                                   int rcount1, int rcount2, int paddingMult) {
        if (paddingMult == 0) return (paddedCount == candCount);
        int mod1 = rcount1 % paddingMult;
        int mod2 = rcount2 % paddingMult;
        int padding1 = (mod1 == 0 ? 0 : paddingMult - mod1);
        int padding2 = (mod2 == 0 ? 0 : paddingMult - mod2);
        // the maximum additional coocs is the minimum of the two paddings.
        int fudgeFactor = (padding2 < padding1 ? padding2 : padding1);
        return (paddedCount >= candCount && paddedCount <= candCount + fudgeFactor);
    }

    /** Count the places where two arrays are both non-zero. */
    private static int countCooccurrences(int[] r1, int[] r2) {
        int count = 0;
        for (int i = 0; i < r1.length; i++) {
            if (r1[i] > 0 && r2[i] > 0) count++;
        }
        return count;
    }
    
    /** Count co-occurrences, omitting padding columns if any. */
    private static int countCoocNoPadding(int[] r1, int[] r2, int padStart) {
        int count = 0;
        for (int i = 0; i < padStart; i++) {
            if (r1[i] > 0 && r2[i] > 0) count++;
        }
        return count;
    }

    /** Count co-occurrences between two rows, simulating partial knowledge 
     *  by skipping a given list of columns. */
    private static int countCoocSkipList(int[] r1, int[] r2, Integer[] skipList) {
        int count = 0;
        int skipIdx = 0;
        for (int i = 0; i < r1.length; i++) {
            if (skipIdx < skipList.length && skipList[skipIdx] == i) {
                skipIdx++;
            }
            else if (r1[i] > 0 && r2[i] > 0) count++;
        }
        return count;
    }
    
    /** Return the number of nonzero entries in an int array. */
    private static int countNonZero(int[] a) {
        int count = 0;
        for (int i=0; i < a.length; i++) {
            if (a[i] != 0) count++;
        }
        return count;
    }

}


