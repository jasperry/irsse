package edu.rutgers.kevin.sse;

import java.io.Reader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.wikipedia.WikipediaTokenizer;
import org.apache.lucene.util.Version;

public class WikipediaAnalyzer extends Analyzer{
	@Override
	protected TokenStreamComponents createComponents(String fieldName,
			Reader reader) {
		WikipediaTokenizer tokenizer = new WikipediaTokenizer(reader);
		LowerCaseFilter filter = new LowerCaseFilter(Version.LUCENE_4_9,
				tokenizer);
		StopFilter sf = new StopFilter(Version.LUCENE_4_9, filter, StandardAnalyzer.STOP_WORDS_SET);
		return new TokenStreamComponents(tokenizer, new PorterStemFilter(
				sf));
	}
}
