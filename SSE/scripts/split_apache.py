### Split apache mailing list files into separate emails.
### Takes list of files on command line, must be in current dir.

import sys
# import re
import os

_, dirname = os.path.split(os.getcwd())
print "In directory " + dirname
outdir = "../" + dirname + "_split"
os.mkdir(outdir)

for fname in sys.argv[1:] :
    infile = open(fname, 'r')
    filenum = 0
    # nothing should get written to file zero.
    # outfile = open("./split/" + fname + "_" + str(filenum).zfill(4), 'w')
    for line in infile:
        if line.startswith("From "): # open next file for writing.
            if filenum > 0:
                outfile.close()
            filenum +=1
            outfile = open(outdir + "/" + fname + "_" + str(filenum).zfill(4), 'w')
        outfile.write(line)
    outfile.close() # just the last one.
    infile.close()
    print "Wrote " + str(filenum) + " emails from file " + fname
