#!/bin/bash

if test -z "$2"
then
   kwfile="keywords.txt"
else
   kwfile=$2
fi

cd bin

java edu.rutgers.kevin.sse.Experiment /.freespace/jasperry/enron/enron_mail_20110402/maildir ../$kwfile 200000 0.00001 $1
