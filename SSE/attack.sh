#!/bin/bash

# DOCIDS="docID_all.ser.gz"

SERVERDATA=tfidf_train_2001.ser.gz
CLIENTDATA=tfidf_test_2001.ser.gz

java -Xms1500m -cp bin edu.rutgers.kevin.sse.IRAttack $SERVERDATA $CLIENTDATA $@

