Notes for running IKK and Count Experiments
===========================================

Building and Obtaining Datasets
-------------------------------

To build everything, make sure Apache Ant is installed, and from the
SSE/ folder run the command

    ant -f build_cmd.xml

(The other build file is used by NetBeans, if you wish to use that.)

The version of the Enron dataset used in this project can be
downloaded from
[here](https://www.cs.cmu.edu/~./enron/enron_mail_20110402.tgz).

NOTE: The email filenames in this archive begin with the dot (.)
character. This has caused problems on Windows machines, and possibly
on Macs as well. It is recommended to run the experiments under Linux.

For the Apache java-user mailing list data, there is a Python script
`split_apache.py` in the scripts/ directory that will take the mailing
list archives and divide it into individual emails for use by the
code.

IKK Attack
----------

For the IKK attack, set the experiment parameters in the file
"ikkexper.properties." Most important is to set the location of the
Enron dataset. The file contains comments explaining what most of the
parameters mean.

To run the IKK attack, see the script "ikkexper.sh", which has the
Java command line.

When the IKK experiment is run for the first time with a given keyword
count, split, and random seed for the first time, it will output a top
keyword file in the current directory. This file will be re-used if
the experiment is run again with the same parameters.

The result of the experiment will be printed to standard output. 

Count Attack
------------

Similarly for the count attack, parameters are set in the file
"countAttack.properties", and the file "countAttack.sh" shows the command line.

The result of the experiment will be printed to standard output.

Document Recovery Statistics
----------------------------

The options are found in "docRecovery.properties" and the command line
in "docRecovery.sh".
